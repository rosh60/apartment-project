@extends('layouts.admin')

@section('title')
Manage Complaints
@stop

@section('content')
<div class="col-md-12">

@if($comps->count() === 0)
<div class="alert alert-info">There are no complaints to display</div>
@endif

@foreach($comps as $cmp)
<div class="panel panel-default complaint" data-id="{{ url('admin/complaints/show/'.$cmp->id) }}" style="cursor:pointer;">
  <div class="panel-heading">{{ $cmp->subject }} <span class="pull-right">By : {{ $cmp->resident->name }}</span></div>
</div>
@endforeach
</div>

<script type="text/javascript">
	$(function() {
		$('.complaint').click(function() {
			window.location = $(this).data('id');
		});
	});
</script>

@stop