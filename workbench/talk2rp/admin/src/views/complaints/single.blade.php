@extends('layouts.admin')

@section('title')
Manage Complaints
@stop

@section('content')
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">{{ $cmp->subject }}
			<p class="pull-right">{{ $cmp->created_at }}</p>
		</div>
		<div class="panel-body">
		    <div class="row">
		        <div class="col-xs-2 col-md-2">
		            <img src="{{ url('images/residents/' . $cmp->resident->photo) }}" width="100%" /><br/>
		            <p class="text-center">{{ $cmp->resident->name }}</p>
		        </div>
		        <div class="col-xs-10 col-md-10">
		            <p>{{ $cmp->complaint }}</p>
		        </div>
		    </div>
		</div> 	
	</div>
@foreach($cmp->replies as $reply)
<div class="well well-sm">
    <div class="row">
        <div class="col-xs-2 col-md-2">
            <img src="{{ url('images/residents/' . $reply->resident->photo) }}" width="100%" /><br/>
            <p class="text-center">{{ $reply->resident->name }}</p>
        </div>
        <div class="col-xs-10 col-md-10">
        	<p class="pull-right">{{ $reply->created_at }}</p>
            <p>{{ $reply->reply }}</p>
        </div>
    </div>
</div>
@endforeach
{{ Form::open() }}
<div class="form-group">
	<textarea class="form-control" rows="3" name="reply"></textarea>
</div>
<button class="btn btn-primary">Post Reply</button>
{{ Form::close() }}
</div>

@stop