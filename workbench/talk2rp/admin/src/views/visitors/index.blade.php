@extends('layouts.admin')

@section('title')
Manage Visitors
@stop

@section('content')

<div class="col-md-12">

<table class="table table-hover table-bordered">
    <tr>
        <th>Flat</th>
        <th>Name</th>
        <th>Contact</th>
        <th>Person to Visit</th>
        <th>Purpose</th>
        <th>Date</th>
    </tr>
@foreach($visitors as $v)
    <tr class="clickable" href="{{ url('admin/visitors/info/'.$v->id) }}" style="cursor:pointer;">
        <td>{{$v->block_number . '-' . $v->door_number}}</td>
        <td>{{$v->name}}</td>
        <td>{{$v->mobile}}</td>
        <td>{{$v->person_to_visit}}</td>
        <td>{{$v->purpose}}</td>
        <td>{{$v->date}}</td>
    </tr>
@endforeach
</table>

    @if($visitors->count() === 0)
    <div class="alert alert-info">There are no visitors to display.</div>
    @endif

</div>


<script type="text/javascript">
    $(function() {
        $('.clickable').click(function() {
            window.location = $(this).attr('href');
        });
    });
</script>

@stop