@extends('layouts.admin')

@section('title')
Manage Residents
@stop

@section('content')

<div class="col-md-12">

<table class="table table-hover table-bordered">
	<tr>
		<th>Flat</th>
		<th>Name</th>
		<th>Contact</th>
		<th>Email</th>
		<th>Intercom</th>
		<th>User Status</th>
		<th>Membership</th>
		<th>Message</th>
	</tr>
    <tbody id="ajaxit">
@foreach($residents as $r)
	<tr class="clickable" href="{{ url('admin/residents/edit/'.$r->id) }}" style="cursor:pointer;">
		<td>{{$r->block_number . '-' . $r->door_number}}</td>
		<td>{{$r->name}}</td>
		<td>{{$r->mobile}}</td>
		<td>{{$r->email}}</td>
		<td>{{$r->intercomm}}</td>
		<td>{{$r->user_status}}</td>
		<td>{{$r->membership or '---'}}</td>
		<td><button class="btn btn-primary btn-xs sendMsg" data-id="{{$r->id}}" data-name="{{$r->name}}" data-toggle="modal" data-target="#msgModal"><span class="glyphicon glyphicon-envelope"></span> &nbsp;Message</button></td>
    </tr>
@endforeach
    </tbody>
</table>

	@if($residents->count() === 0)
	<div class="alert alert-info">There are no staff to display</div>
	@endif

</div>

@include('layouts.messagemodal')

<script>
    $(function() {

        $(document).on('click', '.sendMsg', function(e) {
            $('#name').val($(this).data('name'));
            $('#id').val($(this).data('id'));         
        });

        $(document).on('click', '.clickable', function(e) {
            if(e.target.toString() == '[object HTMLTableCellElement]')
                window.document.location = $(this).attr('href');            
        });

    });

</script>

@stop