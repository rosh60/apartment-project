@extends('layouts.admin')

@section('title')
Manage Residents
@stop

@section('content')

<div class="col-md-12">

<div id="accordion">
<h3>Profile</h3>
<div>
{{ Form::open(['url' => 'admin/residents/edit/' . Input::old('id'), 'files' => true, 'class' => 'form-horizontal']) }}
	<legend>Details of {{ $resident->name }}</legend>
    
    <div class="col-md-5">
	@if($errors->any())
    {{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
    @endif
    @if($message = Session::get('message'))
    {{ '<div class="alert alert-success">'.$message.'</div>' }}
    @endif
    <div class="form-group">
        <label class="col-sm-4">Flat #</label>
        <div class="col-sm-8"><p>{{ $resident->block_number . '-' . $resident->door_number }}</p></div>
    </div>
    <div class="form-group">
   		<label class="col-sm-4">Name</label>
        <div class="col-sm-8">{{ Form::text('name', '', ['class' => 'labelLike'])}}</div>
    </div>
    <div class="form-group">
    	<label class="col-sm-4">DOB</label>
        <div class="col-sm-8">{{ Form::text('dob', '', ['class' => 'labelLike', 'id' => 'dob'])}}</div>
    </div>
    <div class="form-group">
        <label class="col-sm-4">Gender</label>
        <div class="col-sm-8"><p>{{ $resident->gender }}</p></div>
    </div>
    <div class="form-group">
        <label class="col-sm-4">Mobile</label>
        <div class="col-sm-8">{{ Form::text('mobile', '', ['class' => 'labelLike'])}}</div>
    </div>
    <div class="form-group">
    	<label class="col-sm-4">Occupation</label>
        <div class="col-sm-8">{{ Form::text('occupation', '', ['class' => 'labelLike'])}}</div>
    </div>
    <div class="form-group">
    	<label class="col-sm-4">Email</label>
        <div class="col-sm-8">{{ Form::text('email', '', ['class' => 'labelLike'])}}</div>
    </div>
    <div class="form-group">
        <label class="col-sm-4">Blood Group</label>
        <div class="col-sm-8"><p>{{ $resident->blood_group }}</p></div>
    </div>
    <div class="form-group">
        <label class="col-sm-4">Membership</label>
        <div class="col-sm-8">{{ Form::select('membership', ['Select', 'President' => 'President', 'Admin' => 'Admin', 'Vice president' => 'Vice president', 'Secretary' => 'Secretary', 'Treasurer' => 'Treasurer', 'Supervisor' => 'Supervisor', 'Building manager' => 'Building manager'], 'Select', ['class' => 'form-control'])}}</div>
    </div>

    <input type="submit" class="btn btn-primary" style="width:100%" value="Update Information"/>
    </div>

    <div class="col-md-5">
        <div class="form-group"><img src="{{ url('images/residents/'.$resident->photo) }}" class="thumbnail" style="width:320px;" /></div>
        <div class="form-group">
            <label class="col-sm-2">Photo</label>
            <div class="col-sm-3">{{ Form::file('photo') }}</div>
        </div>
    </div>
{{ Form::close() }}
</div>
<h3>Complaints</h3>
<div>

@if($resident->complaint->count() === 0)
<p>No complaints found</p>
@endif

@foreach($resident->complaint as $complaint)
<p><a href="{{ url('admin/complaints/show/'.$complaint->id) }}" />{{$complaint->subject}}</a></p>
@endforeach

</div>
<h3>Payments</h3>
<div>
<p>Nam enim risus, molestie et, porta ac, aliquam ac, risus.
Quisque lobortis.Phasellus pellentesque purus in massa.</p>
</div>

<h3>Vehicles</h3>
<div>
<div class="col-md-4">
{{ Form::open(['url' => 'admin/residents/vehicle', 'files' => true, 'id' => 'vehicleForm']) }}
    <legend>Register Vehicle</legend>
    <div id="ajaxinfo" style="display:none;"></div>
        <div class="form-group">
            {{ Form::text('slot', '', ['class' => 'form-control', 'placeholder' => 'Slot Number'])}}
        </div>
        <div class="form-group">
            {{ Form::text('sticker', '', ['class' => 'form-control', 'placeholder' => 'Sticker Number'])}}
        </div>
        <div class="form-group">
            {{ Form::text('model', '', ['class' => 'form-control', 'placeholder' => 'Make / Model'])}}
        </div>
        <div class="form-group">
            {{ Form::text('number', '', ['class' => 'form-control', 'placeholder' => 'Reg. Number'])}}
        </div>
        <div class="form-group">
            {{ Form::select('type', ['Select', '2 Wheeler' => '2 Wheeler', '4 Wheeler' => '4 Wheeler'], 'Select', ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            <label>Front Image</label>
            {{ Form::file('front', ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            <label>Back Image</label>
            {{ Form::file('back', ['class' => 'form-control'])}}
        </div>
        {{ Form::hidden('resident', $resident->id) }}
        <input type="submit" class="btn btn-primary btn-lg" value="Register Vehicle" style="width:100%"/>
{{ Form::close() }}
</div>
<div class="col-md-8">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Slot #</th>
                <th>Sticker #</th>
                <th>Model</th>
                <th>Reg. Number</th>
                <th>Vehicle Type</th>
            </tr>
        </thead>
        <tbody>
        @foreach($resident->vehicles as $vehicle)
            <tr>
                <td>{{ $vehicle->slot }}</td>
                <td>{{ $vehicle->sticker }}</td>
                <td>{{ $vehicle->model }}</td>
                <td>{{ $vehicle->number }}</td>
                <td>{{ $vehicle->type }}</td>
                <td><a href="{{ action('Admin\Controllers\VehicleController@getDelete', $vehicle->id) }}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>
</div>

<script src="http://malsup.github.com/jquery.form.js"></script>
<script>
$(function() {
	$('#accordion').accordion({
		heightStyle: "content"
	});
    $('#dob').datepicker({ dateFormat: 'dd-mm-yy' });

    $('#vehicleForm').ajaxForm({
        success: function(rsp)
        {
            if(rsp.ret === 0)
                $('#ajaxinfo').text(rsp.msg).removeClass().addClass('alert alert-danger').fadeIn();
            else
            {
                alert('Registration successful.');
                location.reload();
            }
        }
    });

});
</script>

</div>

@stop