@extends('layouts.admin')

@section('title')
Manage Residents
@stop

@section('content')

<SCRIPT FOR="GrFingerX" EVENT="ImageAcquired(id, w, h, rawImg, res)" LANGUAGE="javascript">
GrFingerX.CapSaveRawImageToFile(rawImg, w, h, "D:\\teste.bmp", 501);
Start();
if(document.getElementById('img').style.display == 'none')
    document.getElementById('img').style.display = 'block';
CallEnroll(rawImg, w, h, res);
</SCRIPT>

<SCRIPT language="JavaScript" type="text/javascript">
<!--
function Start() {
document.getElementById("img").src = "D:\\teste.bmp?ts" + encodeURIComponent( new Date().toString() );
}
</SCRIPT> 

<div class="col-md-12">

<div id="tabs">
<ul>
<li><a href="#tabs-1">Resident List</a></li>
<li><a href="#tabs-2">New Resident</a></li>
<li class="pull-right"><a style="padding:5px 0 0 0;border:0;" href="#tabs-1"><input type="text" id="resSearch" placeholder="Search..." style="border:0"/></a></li>
</ul>

<div id="tabs-1">
<table class="table table-hover table-bordered">
	<tr>
		<th>Flat</th>
		<th>Name</th>
		<th>Contact</th>
		<th>Email</th>
		<th>Intercom</th>
		<th>User Status</th>
		<th>Membership</th>
		<th>Message</th>
	</tr>
    <tbody id="ajaxit">
@foreach($residents as $r)
	<tr class="clickable" href="{{ url('admin/residents/edit/'.$r->id) }}" style="cursor:pointer;">
		<td>{{$r->block_number . '-' . $r->door_number}}</td>
		<td>{{$r->name}}</td>
		<td>{{$r->mobile}}</td>
		<td>{{$r->email}}</td>
		<td>{{$r->intercomm}}</td>
		<td>{{$r->user_status}}</td>
		<td>{{$r->membership or '---'}}</td>
		<td><button class="btn btn-primary btn-xs sendMsg" data-id="{{$r->id}}" data-name="{{$r->name}}" data-toggle="modal" data-target="#msgModal"><span class="glyphicon glyphicon-envelope"></span> &nbsp;Message</button></td>
    </tr>
@endforeach
    </tbody>
</table>

    @if($residents->count() === 0)
    <div class="alert alert-info">There are no residents to display.</div>
    @endif

<div class="text-center">{{ $residents->links() }}</div>
</div>

<div id="tabs-2">

{{ Form::open(['url' => 'admin/residents', 'class' => 'form-horizontal', 'id' => 'addResident']) }}
	<legend>Register Resident</legend>
	<div class="col-md-5">

	<div id="ajaxResult" style="display:none;"></div>

	<div class="form-group">
		<div class="col-sm-4">
		<label>Flat #:</label>
    	</div>
		<div class="col-sm-3">
			{{ Form::select('block_number', ['Block', 'A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E', 'F' => 'F'], 'Block', ['class' => 'form-control'])}}
		</div>
		<div class="col-sm-5">
			{{ Form::text('door_number', '', ['class' => 'form-control', 'placeholder' => 'Door Number', 'maxlength' => 3])}}
		</div>
	</div>

    <div class="form-group">
    	<label class="col-sm-4">Name</label>
        <div class="col-sm-8">{{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}</div>
    </div>

    <div class="form-group">
    	<label class="col-sm-4">Date of Birth</label>
        <div class="col-sm-8">{{ Form::text('dob', '', ['class' => 'form-control', 'placeholder' => 'Date of Birth', 'id' => 'dob'])}}</div>
    </div>

    <div class="form-group">
    	<label class="col-sm-4">Gender</label>
        <div class="col-sm-8">
        	<label class="radio-inline">
			  <input type="radio" name="gender" value="Male"> Male
			</label>
			<label class="radio-inline">
			  <input type="radio" name="gender" value="Female"> Female
			</label>
        </div>
    </div>

    <div class="form-group">
    	<label class="col-sm-4">Blood Group</label>
        <div class="col-sm-8">{{ Form::select('blood_group', ['Blood Group', 'A Positive' => 'A Positive', 'A Negetive' => 'A Negetive', 'B Positive' => 'B Positive', 'B Negetive' => 'B Negetive', 'AB Postive' => 'AB Postive', 'AB Negetive' => 'AB Negetive', 'O Positive' => 'O Positive', 'O Negetive' => 'O Negetive'], 'Blood Group', ['class' => 'form-control'])}}</div>
    </div>

    <div class="form-group">
    	<label class="col-sm-4">Mobile</label>
        <div class="col-sm-8">{{ Form::text('mobile', '', ['class' => 'form-control', 'placeholder' => 'Mobile Number', 'maxlength' => 10])}}</div>
    </div>

    <div class="form-group">
        <label class="col-sm-4">Intercomm</label>
        <div class="col-sm-8">{{ Form::text('intercomm', '', ['class' => 'form-control', 'placeholder' => 'Intercomm Number', 'maxlength' => 3])}}</div>
    </div>

    <div class="form-group">
        <label class="col-sm-4">User Status</label>
        <div class="col-sm-8">
            <label class="radio-inline">
              <input type="radio" name="user_status" value="Owner"> Owner
            </label>
            <label class="radio-inline">
              <input type="radio" name="user_status" value="Tenant"> Tenant
            </label>
        </div>
    </div>

    <div class="form-group">
    	<label class="col-sm-4">Occupation</label>
        <div class="col-sm-8">{{ Form::text('occupation', '', ['class' => 'form-control', 'placeholder' => 'Occupation'])}}</div>
    </div>

    <div class="form-group">
    	<label class="col-sm-4">Email ID</label>
        <div class="col-sm-8">{{ Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'E-mail ID'])}}</div>
    </div>
    <input type="submit" class="btn btn-primary btn-lg" value="Register Resident" style="width:100%"/>

    </div>

    <div class="col-md-4">
    	<div id="webcam" style="">
        </div><br/>
        <a class="btn btn-primary" id="btn1" onclick="base64_tofield();base64_toimage()" style="width:320px">Take Snapshot</a>
        <div style="width:200px;"><br>
            <img id="image" />
        
        </div>
        {{ Form::hidden('photo', '', ['id' => 'formfield']) }}
    </div>

    <div class="col-md-3 text-center">
	    <IMG id="img" src="http://www.generatorstudios.com/fingerprint.gif" border="1" name="refresh" /><br/>
	    <input type = "button" class="btn btn-primary" value = "Capture" onclick = "Initialize()" />
	    {{ Form::hidden('fingerprint', '', ['id' => 'fingerprint'])}}
	    <textarea name="log" id = "log"></textarea>
	</div>

{{ Form::close() }}

<div class="clearfix"></div>
</div>

</div>

</div>

@include('layouts.messagemodal')

<script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
{{ HTML::script('js/scriptcam.js') }}

<script language="JavaScript">
    $(document).ready(function() {
        $("#webcam").scriptcam({
            showMicrophoneErrors:false,
            onError:onError,
            cornerRadius:20,
            cornerColor:'e3e5e2',
            onWebcamReady:onWebcamReady,
            onPictureAsBase64:base64_tofield_and_image,
            path:'{{ URL::to("") }}/js/'
        });
    });
    function base64_tofield() {
        $('#formfield').val($.scriptcam.getFrameAsBase64());
    };
    function base64_toimage() {
        $('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64()).slideDown();
    };
    function base64_tofield_and_image(b64) {
        $('#formfield').val(b64);
        $('#image').attr("src","data:image/png;base64,"+b64);
    };
    function changeCamera() {
        $.scriptcam.changeCamera($('#cameraNames').val());
    }
    function onError(errorId,errorMsg) {
        $( "#btn1" ).attr( "disabled", true );
        $( "#btn2" ).attr( "disabled", true );
        alert(errorMsg);
    }
    function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
        $.each(cameraNames, function(index, text) {
            $('#cameraNames').append( $('<option></option>').val(index).html(text) )
        });
        $('#cameraNames').val(camera);
    }
</script>

<script>
    $(function() {
		$('#tabs').tabs();
		$('#dob').datepicker({ dateFormat: 'dd-mm-yy' });

        $('#resSearch').keyup(function() {
            if($('#resSearch').val().length >= 2)
            {
                $.ajax({
                    url: '{{ url('admin/residents/search') }}',
                    type: 'GET',
                    data: {term: $('#resSearch').val()},
                    success: function(rsp)
                    {
                        var i = 0;
                        var url = '{{ url('admin/residents/edit/') }}';
                        $('#ajaxit').html('');
                        while(typeof rsp[i] != 'undefined')
                        {
                            $('#ajaxit').html($('#ajaxit').html() + 
                                '<tr class="clickable" href="' + url + '/' + rsp[i].id + '" style="cursor:pointer;">' +
                                '<td>' + rsp[i].block_number + '-' + rsp[i].door_number + '</td>' +
                                '<td>' + rsp[i].name + '</td>' +
                                '<td>' + rsp[i].mobile + '</td>' +
                                '<td>' + rsp[i].email + '</td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td>' + rsp[i].membership + '</td>' +
                                '<td><button class="btn btn-primary btn-xs sendMsg" data-id="'+ rsp[i].id +'" data-name="'+ rsp[i].name +'" data-toggle="modal" data-target="#msgModal"><span class="glyphicon glyphicon-envelope"></span> &nbsp;Message</button></td>' +
                                '</tr>'
                                );
                            i++;
                        }
                    }
                });
            }
           
        });

		$('#addResident').submit(function() {
			var fields = $(this).serialize();
			$.ajax({
				url: $(this).attr('action'),
				type: 'POST',
				dataType: 'json',
				data: fields,
				statusCode: {
					200: function(res){
						$('#ajaxResult').removeClass().addClass('alert alert-danger').html(res.responseText).fadeIn();
					},
					201: function(res){
						$('#ajaxResult').removeClass().addClass('alert alert-success').html(res.responseText).fadeIn();
					}
				}
			})
			.fail(function() {
				$('#ajaxResult').removeClass().addClass('alert alert-danger').html('Error processing the request.').fadeIn();
			});
			return false;
		});

        $(document).on('click', '.sendMsg', function(e) {
            $('#name').val($(this).data('name'));
            $('#id').val($(this).data('id'));         
        });

        $(document).on('click', '.clickable', function(e) {
            if(e.target.toString() == '[object HTMLTableCellElement]')
                window.document.location = $(this).attr('href');            
        });

    });

</script>

@stop