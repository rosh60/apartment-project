@extends('layouts.admin')

@section('title')
Admin Application
@stop

@section('content')

<div class="col-md-10">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Slot #</th>
                <th>Sticker #</th>
                <th>Model</th>
                <th>Reg. Number</th>
                <th>Vehicle Type</th>
                <th>Front Image</th>
                <th>Back Image</th>
            </tr>
        </thead>
        <tbody>
        @foreach($vehicles as $vehicle)
            <tr>
                <td>{{ $vehicle->slot }}</td>
                <td>{{ $vehicle->sticker }}</td>
                <td>{{ $vehicle->model }}</td>
                <td>{{ $vehicle->number }}</td>
                <td>{{ $vehicle->type }}</td>
                <td>
	                <a href="{{ url('images/vehicles/' . $vehicle->front) }}">
	                	<img src="{{ url('images/vehicles/' . $vehicle->front) }}" height="20%" />
	                </a>
                </td>
                <td>
	                <a href="{{ url('images/vehicles/' . $vehicle->back) }}">
	                	<img src="{{ url('images/vehicles/' . $vehicle->back) }}" height="20%" />
	                </a>
                </td>
                <td><a href="{{ action('Admin\Controllers\VehicleController@getDelete', $vehicle->id) }}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

	@if($vehicles->count() === 0)
	<div class="alert alert-info">There are no vehicles to display</div>
	@endif

</div>


@stop