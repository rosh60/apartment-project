@extends('layouts.admin')

@section('title')
Manage Staff
@stop

@section('content')

<div class="row">
<div class="col-md-6" style="border-right:1px solid #ccc;">
{{ Form::open(['url' => 'admin/staff/edit/'.$staff->id]) }}
	<legend>{{ $staff->name }}'s Details <a class="btn btn-primary btn-sm pull-right" href="{{ action('Admin\Controllers\StaffController@getDelete', $staff->id) }}"><span class="glyphicon glyphicon-remove"></span>&nbsp;Delete Account</a></legend>
    @if($errors->any())
    	{{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
    @endif
    @if($message = Session::get('message'))
    	{{ '<div class="alert alert-success">'.$message.'</div>' }}
    @endif
    <div class="row">
	<div class="col-md-7">
	    <div class="form-group">
	        {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
	    </div>
	    <div class="form-group">
	        {{ Form::select('category', ['Category', 'maid' => 'Maid', 'driver' => 'Driver'], 'Maid', ['class' => 'form-control'])}}
	    </div>
	    <div class="form-group">
	        {{ Form::select('gender', ['Gender', 'male' => 'Male', 'female' => 'Female'], '', ['class' => 'form-control', 'readonly'])}}
	    </div>
    </div>
    <div class="col-md-5">
    	<img class="thumbnail" src="{{ url('images/staff/'.$staff->photo) }}" style="height:135px" />
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
	    <div class="form-group">
	        {{ Form::text('dob', '', ['class' => 'form-control', 'placeholder' => 'Date of birth', 'readonly'])}}
	    </div>
	    <div class="form-group">
	        {{ Form::text('mobile', '', ['class' => 'form-control', 'placeholder' => 'Mobile Number'])}}
	    </div>
	    <div class="form-group">
	        {{ Form::textarea('address', '', ['class' => 'form-control', 'rows' => 4, 'placeholder' => 'Address'])}}
	    </div>
	    <input type="submit" class="btn btn-primary btn-lg" value="Update Information" style="width:100%"/>
	</div>
	</div>
{{ Form::close() }}
</div>

<div class="col-md-5">
	<div style="padding: 30px; border-bottom:1px solid #ccc">
		Assign to : <input type="text" id="txtAssign" /><div id="namedrop"></div>
		{{ Form::hidden('to', '', ['id' => 'to'])}}
		<button id="btnAssign" class="btn btn-primary btn-sm">Assign</button>
	</div>
	<div style="padding:10px;">
	{{ \Input::old('name') }} is currently assigned to :<br />
	@foreach($staff->residents as $res)
	<div style="padding:5px">
		<img src="{{ url('images/residents/'.$res->photo) }}" width="50px" />
		{{ $res->name . ' | Door #: ' . $res->door_number . '-' . $res->block_number}} <a class="btn btn-default pull-right glyphicon glyphicon-remove" href="{{ url('admin/staff/revoke-staff/'.$res->id.'/'.$staff->id) }}"></a><br/>
	</div>
	@endforeach
	</div>
</div>
</div>


<script>
$(function() {

	$('#txtAssign').keyup(function() {
		if($(this).val().length >= 2)
		{
			$.ajax({
				url: '{{ url('api/ajaxidsearch') }}',
				type: 'GET',
				dataType: 'json',
				data: {search: $(this).val()},
				success: function(response){
              	$('#namedrop').html('');
	             	for (var i = 0; i < response.length; i++) {
	                 	$('#namedrop').html($('#namedrop').html() + '<span class="element" id="'+response[i].id+'">' + response[i].name + '</span>');
	                }
	                $('#namedrop').fadeIn();
             	}
           });
		}
	});

	$('#namedrop').on('click', ".element", function(){
      $('#txtAssign').val($(this).html());
      $('#to').val($(this).attr('id'));
      $('#namedrop').fadeOut();
  	});

  	$('#btnAssign').click(function(event) {
  		$.post('{{ url('admin/staff/assign-staff') }}', { resident: $('#to').val(), staff: {{ $staff->id }} }, function(data) {
	  		location.reload();
  		});
  	});
	
});
</script>

@stop