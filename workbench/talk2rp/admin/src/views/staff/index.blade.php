@extends('layouts.admin')

@section('title')
Manage Staff
@stop

@section('content')

<div class="col-md-12">

<div id="tabs">
<ul>
<li><a href="#tabs-1">Staff List</a></li>
<li><a href="#tabs-2">New Staff</a></li>
</ul>

<div id="tabs-1">
<table class="table table-hover table-bordered">
	<tr>
		<th>Name</th>
		<th>Category</th>
		<th>Gender</th>
		<th>DOB</th>
		<th>Mobile</th>
		<th>Address</th>
	</tr>
@foreach($staff as $s)
	<tr class="clickable" href="{{ url('admin/staff/edit/'.$s->id) }}" style="cursor:pointer;">
		<td>{{$s->name}}</td>
		<td>{{$s->category}}</td>
		<td>{{$s->gender}}</td>
		<td>{{$s->dob}}</td>
		<td>{{$s->mobile}}</td>
		<td>{{$s->address}}</td>
    </tr>
@endforeach
</table>

	@if($staff->count() === 0)
	<div class="alert alert-info">There are no staff members to display.</div>
	@endif

<div class="text-center">{{ $staff->links() }}</div>
</div>

<div id="tabs-2">
{{ Form::open(['url' => 'admin/staff', 'id' => 'addStaff']) }}
	<legend>Register new staff</legend>
	<div class="col-md-5">
		<div id="ajaxResult" style="display:none;"></div>
	    <div class="form-group">
	        {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
	    </div>
	    <div class="form-group">
	        {{ Form::select('category', ['Category', 'maid' => 'Maid', 'driver' => 'Driver'], 'Maid', ['class' => 'form-control'])}}
	    </div>
	    <div class="form-group">
	        {{ Form::select('gender', ['Gender', 'male' => 'Male', 'female' => 'Female'], '', ['class' => 'form-control'])}}
	    </div>
	    <div class="form-group">
	        {{ Form::text('dob', '', ['class' => 'form-control', 'placeholder' => 'Date of birth', 'id' => 'dob', 'autocomplete' => 'off'])}}
	    </div>
	    <div class="form-group">
	        {{ Form::text('mobile', '', ['class' => 'form-control', 'placeholder' => 'Mobile Number', 'maxlength' => 10])}}
	    </div>
	    <div class="form-group">
	        {{ Form::textarea('address', '', ['class' => 'form-control', 'rows' => 4, 'placeholder' => 'Address'])}}
	    </div>
	    <input type="submit" class="btn btn-primary btn-lg" value="Register Staff" style="width:100%"/>
    </div><!-- End of col-md 5 -->

    <div class="col-md-4">
    	<div id="webcam" style="">
        </div><br/>
        <a class="btn btn-primary" id="btn1" onclick="base64_tofield();base64_toimage()" style="width:320px">Take Snapshot</a>
        <div style="width:200px;"><br>
            <img id="image" />
        </div>
        {{ Form::hidden('photo', '', ['id' => 'formfield']) }}
    </div><!-- End of col-md 4 -->

    <div class="col-md-3 text-center">
	    <IMG id="img" src="http://www.generatorstudios.com/fingerprint.gif" border="1" name="refresh" /><br/>
	    <input type = "button" class="btn btn-primary" value = "Capture" onclick = "Initialize()" />
	    {{ Form::hidden('fingerprint', '', ['id' => 'fingerprint'])}}
	    <textarea name="log" id = "log"></textarea>
	</div><!-- End of col-md 4 -->
{{ Form::close() }}
<div class="clearfix"></div>
</div>

</div>
</div>

<script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
{{ HTML::script('js/scriptcam.js') }}

<script language="JavaScript">
    $(document).ready(function() {
        $("#webcam").scriptcam({
            showMicrophoneErrors:false,
            onError:onError,
            cornerRadius:20,
            cornerColor:'e3e5e2',
            onWebcamReady:onWebcamReady,
            onPictureAsBase64:base64_tofield_and_image,
            path:'{{ URL::to("") }}/js/'
        });
    });
    function base64_tofield() {
        $('#formfield').val($.scriptcam.getFrameAsBase64());
    };
    function base64_toimage() {
        $('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64()).slideDown();
    };
    function base64_tofield_and_image(b64) {
        $('#formfield').val(b64);
        $('#image').attr("src","data:image/png;base64,"+b64);
    };
    function changeCamera() {
        $.scriptcam.changeCamera($('#cameraNames').val());
    }
    function onError(errorId,errorMsg) {
        $( "#btn1" ).attr( "disabled", true );
        $( "#btn2" ).attr( "disabled", true );
        alert(errorMsg);
    }
    function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
        $.each(cameraNames, function(index, text) {
            $('#cameraNames').append( $('<option></option>').val(index).html(text) )
        });
        $('#cameraNames').val(camera);
    }
</script>

<script>
$(function() {
	$('#tabs').tabs();
	$('#dob').datepicker({ dateFormat: 'dd-mm-yy' });

	$('#addStaff').on('submit', function()
	{
		var fields = $(this).serialize();
		$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			dataType: 'json',
			data: fields,
			statusCode: {
				200: function(res){
					$('#ajaxResult').removeClass().addClass('alert alert-danger').html(res.responseText).fadeIn();
				},
				201: function(res){
					$('#ajaxResult').removeClass().addClass('alert alert-success').html(res.responseText).fadeIn();
				}
			}
		});
		return false;
	});

	$('.clickable').on('click', function(){
		window.document.location = $(this).attr('href');
	});
});
</script>

@stop