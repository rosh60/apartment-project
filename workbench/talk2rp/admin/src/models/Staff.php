<?php

namespace Admin\Models;

class Staff extends \Eloquent
{
	protected $table = 'staff';
	protected $softDelete = true;
	protected $fillable = ['name', 'category', 'gender', 'dob', 'mobile', 'photo', 'address'];
	public static $rules = [
						'name' 		=> 'required',
						'category' 	=> 'required|in:driver,maid',
						'gender' 	=> 'required|in:male,female',
						'dob' 		=> 'required|date',
						'mobile' 	=> 'required|digits:10',
						'address' 	=> 'required',
						'photo'		=> 'required'
					];

	public function residents()
	{
		return $this->belongsToMany('Resident', 'resident_staff');
	}


}