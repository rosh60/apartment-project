<?php

namespace Admin\Models;

class Reply extends \Eloquent
{
	protected $table = 'replies';
	protected $guarded = ['id'];

	public function complaint()
	{
		return $this->belongsTo('Complaint', 'complaint_id');
	}

	public function resident()
	{
		return $this->belongsTo('Resident', 'uid');
	}


}