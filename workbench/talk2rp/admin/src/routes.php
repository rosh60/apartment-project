<?php

/**
 * Routes for admin application
 */

Route::group(array('before' => 'admin'), function()
{
	Route::controller('admin/residents/vehicle', 'Admin\Controllers\VehicleController');
	Route::controller('admin/residents', 'Admin\Controllers\ResidentController');
	Route::controller('admin/staff', 'Admin\Controllers\StaffController');
	Route::controller('admin/visitors', 'Admin\Controllers\VisitorController');
	Route::controller('admin/complaints', 'Admin\Controllers\ComplaintController');
	Route::controller('admin', 'Admin\Controllers\AdminController');
});