<?php

namespace Admin\Controllers;

Class VehicleController extends \Controller
{

	public function getIndex()
	{
		$vehicles = \Vehicle::paginate(10);
		return \View::make('admin::vehicles.index', compact('vehicles'));
	}

	public function postIndex()
	{
		$input = \Input::all();
		$v = \Validator::make($input, \Vehicle::$rules);
		if($v->fails()) return \Response::make(['ret' => 0, 'msg' => $v->errors()->first()], 200);

		$path = public_path('images/vehicles');
		$file1 = \Str::random(10) . '.jpg';
		$file2 = \Str::random(10) . '.jpg';
		\Input::file('front')->move($path, $file1);
		\Input::file('back')->move($path, $file2);

		$r = \Resident::findOrFail($input['resident']);
		$vehicle = new \Vehicle();
		$vehicle->slot 		= \Common::sanitize($input['slot']);
		$vehicle->sticker 	= \Common::sanitize($input['sticker']);
		$vehicle->model 	= \Common::sanitize($input['model']);
		$vehicle->number 	= \Common::sanitize($input['number']);
		$vehicle->type 		= $input['type'];
		$vehicle->front 	= $file1;
		$vehicle->back 		= $file2;
		$r->vehicles()->save($vehicle);
		return \Response::make(['ret' => 1, 'msg' => 'Registration Successful.'], 200);
	}

	public function getDelete($id)
	{
		$vehicle = \Vehicle::findOrFail($id)->delete();
		return \Redirect::back();
	}

}