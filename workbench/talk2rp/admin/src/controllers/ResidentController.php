<?php

namespace Admin\Controllers;

class ResidentController extends \Controller
{

	public function getIndex()
	{
		$residents = \Resident::orderBy('block_number')->orderBy('door_number')->paginate(10);
		return \View::make('admin::residents.showall', compact('residents'));
	}

	public function postIndex()
	{
		$input = \Input::all();
		$v = \Validator::make($input, \Resident::$rules);
		if($v->fails())
			return \Response::make($v->errors()->first(), 200);

		$resident = new \Resident();
		$resident->block_number = $input['block_number'];
		$resident->door_number = $input['door_number'];
		$resident->name = \Common::sanitize($input['name']);
		$resident->dob = $input['dob'];
		$resident->gender = $input['gender'];
		$resident->mobile = $input['mobile'];
		$resident->intercomm = $input['intercomm'];
		$resident->user_status = $input['user_status'];
		$resident->email = $input['email'];
		$resident->blood_group = $input['blood_group'];
		$resident->occupation = \Common::sanitize($input['occupation']);
		$resident->otp = \Resident::generateOtp($input['name'], $input['block_number'], $input['door_number'], $input['dob']);
		$resident->id = \Resident::generateUid($resident->otp);
		$resident->membership = 'Other';
		file_put_contents(public_path('images/residents/'.$resident->id.'.jpg'), base64_decode($input['photo']));
		$resident->photo = $resident->id.'.jpg';
		$resident->save();
		return \Response::make('Registration successful.', 201);
	}

	public function getEdit($id)
	{
		$resident = \Resident::with('complaint', 'vehicles')->findOrFail($id);
		\Session::flashInput($resident->toArray());
		return \View::make('admin::residents.edit', compact('resident'));
	}

	public function postEdit($id)
	{
		$rules = array(
				'name' => 'required',
				'dob' => 'date|required',
				'mobile' => 'required|digits:10',
				'occupation' => 'required',
				'email' => 'required|email'
			);

		$v = \Validator::make(\Input::all(), $rules);
		if($v->fails()) return \Redirect::back()->withInput()->withErrors($v);

		$r = \Resident::findOrFail($id);
		$r->name = \Common::sanitize(\Input::get('name'));
		$r->dob = \Input::get('dob');
		$r->mobile = \Input::get('mobile');
		$r->occupation = \Common::sanitize(\Input::get('occupation'));
		$r->email = \Input::get('email');
		$r->membership = \Input::get('membership');

		if(\Input::hasFile('photo'))
		{
			\Input::file('photo')->move(public_path('images/residents'), $r->id . '.jpg');
			$r->photo = $r->id . '.jpg';
		}

		$r->update();
		return \Redirect::back()->with('message', 'Success.');
	}

	public function getHistory()
	{
		$residents = \Resident::onlyTrashed()->paginate(10);
		return \View::make('admin::residents.common', compact('residents'));
	}

	public function getSearch()
	{
		$term = \Common::sanitize(\Input::get('term'));
		$residents = \Resident::where('name', 'LIKE', "%$term%")->get();
		return \Response::make($residents, 200);		
	}

}