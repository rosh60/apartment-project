<?php

namespace Admin\Controllers;

class AdminController extends \Controller
{

	public function getIndex()
	{
		return \View::make('admin::index');
	}

	public function getMembershipCommittee()
	{
		$residents = \Resident::where('membership', '<>', 'Other')->paginate(10);
		return \View::make('admin::residents.common', compact('residents'));
	}

}