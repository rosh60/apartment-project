<?php

namespace Admin\Controllers;

use Admin\Models\Staff as Staff;

class StaffController extends \Controller
{

	public function getIndex()
	{
		$staff = Staff::paginate(10);
		return \View::make('admin::staff.index', compact('staff'));
	}

	public function postIndex()
	{
		$v = \Validator::make(\Input::all(), Staff::$rules);
		if($v->fails()) return \Response::make($v->errors()->first(), 200);

		$filename = \Str::random(10) . '.jpg';
		\File::put(public_path('images/staff/'.$filename), base64_decode(\Input::get('photo')));

		$s = new Staff(\Input::all());
		$s->photo = $filename;
		$s->save();
		return \Response::make('Registration successful.', 201);
	}

	public function getEdit($id)
	{
		$staff = Staff::with('residents')->findOrFail($id);
		\Session::flashInput($staff->toArray());
		return \View::make('admin::staff.edit', compact('staff'));
	}

	public function postEdit($id)
	{
		$input = \Input::all();
		$v = \Validator::make($input, array_except(Staff::$rules, 'photo'));
		if($v->fails()) return \Redirect::to('admin/staff/edit/'.$id)->withErrors($v)->withInput();

		$staff = Staff::findOrFail($id);
		$staff->name = $input['name'];
		$staff->category = $input['category'];
		$staff->mobile = $input['mobile'];
		$staff->address = $input['address'];
		$staff->update();
		return \Redirect::to('admin/staff/edit/'.$id)->with('message', 'Updated.');
	}

	public function postAssignStaff()
	{
		$res = \Resident::findOrFail(\Input::get('resident'));
		if(!$res->staff->contains(\Input::get('staff')))
			$res->staff()->attach(\Input::get('staff'));
		return \Response::make('success', 200);
	}

	public function getRevokeStaff($resident, $staff)
	{
		$res = \Resident::findOrFail($resident);
		$res->staff()->detach($staff);
		return \Redirect::back();
	}

	public function getDelete($id)
	{
		$staff = Staff::findOrFail($id);
		$staff->delete();
		return \Redirect::to('admin/staff');
	}

}