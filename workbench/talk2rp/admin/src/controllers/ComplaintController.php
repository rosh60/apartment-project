<?php

namespace Admin\Controllers;

use Admin\Models\Reply as Reply;

class ComplaintController extends \Controller
{

	public function getIndex()
	{
		$comps = \Complaint::with('resident')->get();
		return \View::make('admin::complaints.index', compact('comps'));
	}

	public function getShow($id)
	{
		$cmp = \Complaint::with('resident', 'replies')->with('replies.resident')->find($id);
		return \View::make('admin::complaints.single', compact('cmp'));
	}

	public function postShow($id)
	{
		if(\Input::get('reply') != null)
		{
			$reply = new Reply(\Input::all());
			$reply->uid = \Auth::user()->id;

			$cmp = \Complaint::findOrFail($id);
			$cmp->replies()->save($reply);
			return \Redirect::back();
		}
	}

}