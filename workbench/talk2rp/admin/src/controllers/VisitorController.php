<?php

namespace Admin\Controllers;

Class VisitorController extends \Controller
{

	public function getIndex()
	{
		$visitors = \Visitor::all();
		return \View::make('admin::visitors.index', compact('visitors'));
	}

	public function getInfo()
	{
		return \View::make('admin::visitors.single');
	}

}