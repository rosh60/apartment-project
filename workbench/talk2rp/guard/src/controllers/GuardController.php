<?php

namespace Talk2rp\Guard\Controllers;

class GuardController extends \Controller
{

	public function getLogin()
	{
		return \View::make('guard::login', ['title' => 'Guard Login']);
	}

}