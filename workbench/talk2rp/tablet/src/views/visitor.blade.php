@extends('layouts.tablet')

@section('content')

<SCRIPT FOR="GrFingerX" EVENT="ImageAcquired(id, w, h, rawImg, res)" LANGUAGE="javascript">
GrFingerX.CapSaveRawImageToFile(rawImg, w, h, "D:\\teste.bmp", 501);
Start();
if(document.getElementById('img').style.display == 'none')
    document.getElementById('img').style.display = 'block';
CallEnroll(rawImg, w, h, res);
</SCRIPT>

<div class="span12 centerdiv">
{{ Form::open(['url' => 'visitor/add', 'method' => 'post', 'class' => 'form-horizontal']) }}
<legend>New Visitor</legend>
    @if($errors->any())
    {{ '<div class="alert alert-error">'.implode('<br>', $errors->all()).'</div>' }}
    @endif
    @if($message = Session::get('message'))
    {{ '<div class="alert alert-success">'.$message.'</div>' }}
    @endif
    <div style="width:330px;float:left;">
        <div id="webcam" style="">
        </div>
        <a class="btn btn-small" id="btn1" onclick="base64_tofield();base64_toimage()">Take Snapshot</a>
        <div style="width:200px;"><br>
        <img id="image" style="width:200px;"/>

        <input type = "button" value = "Take Fingerprint" onclick = "Initialize()" />

        <IMG id="img" style="display: none;" border="1" name="refresh" />
        <SCRIPT language="JavaScript" type="text/javascript">
        <!--
        function Start() {
        document.getElementById("img").src = "D:\\teste.bmp?ts" + encodeURIComponent( new Date().toString() );
        }
        </SCRIPT> 
        {{ Form::hidden('fingerprint', '', ['id' => 'fingerprint'])}}
        <textarea name="log" id = "log" rows = "5" cols = "75" ></textarea>
        
        </div>
        {{ Form::textarea('photo', '', ['id' => 'formfield', 'style' => 'display:block; visibility:hidden;']) }}
    </div>


<fieldset>
    <div class="control-group">
        <label class="control-label">Block & Door No.<span style="color:red">*</span></label>
        <div class="controls">
            {{ Form::select('block_number', [
            '' => 'Block',
            'A' => 'A',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D',
            'E' => 'E',
            'F' => 'F'
            ], 'Block', ['id' => 'block_number', 'class' => 'input-small', 'required' => '']) }}
            {{ Form::text('door_number', '', ['id' => 'door_number', 'required' => '', 'maxlength' => '3', 'placeholder' => 'Door Number', 'class' => 'input-small', 'autocomplete' => 'off']) }}<div id="namelist"><span class="name">Person to visit ?</span></div>
            <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
<!--        <label class="control-label">Person to visit<span style="color:red">*</span></label>-->
        <div class="controls">
            <div id="namelist" style="display: none;"></div>
            {{ Form::hidden('person_to_visit', '', ['id' => 'tovisit', 'class' => 'input-xlarge'])}}
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Visitor Name<span style="color:red">*</span></label>
        <div class="controls">
            {{ Form::text('visitor_name', '', ['required' => '', 'placeholder' => 'Full Name', 'class' => 'input-xlarge'])}}
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Purpose of visit<span style="color:red">*</span></label>
        <div class="controls">
            {{ Form::text('purpose', '', ['required' => '', 'placeholder' => 'Purpose of Visit', 'class' => 'input-xlarge'])}}
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Address / Coming From<span style="color:red">*</span></label>
        <div class="controls">
            {{ Form::text('address', '', ['required' => '', 'placeholder' => 'Address / Coming From', 'class' => 'input-xlarge'])}}
            <p class="help-block">Street address, P.O. box, company name, c/o</p>
        </div>
    </div>
    <!-- city input-->
    <div class="control-group">
        <label class="control-label">Mobile<span style="color:red">*</span></label>
        <div class="controls">
            {{ Form::text('mobile', '', ['required' => '', 'placeholder' => 'Mobile Number', 'class' => 'input-xlarge']) }}
            <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Gender</label>
        <div class="controls">
            <label class="radio inline">
                {{ Form::radio('gender', '1') }}
                Male
            </label>
            <label class="radio inline">
                {{ Form::radio('gender', '0') }}
                Female
            </label>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Vehicle Number</label>
        <div class="controls">
		{{ Form::text('vehicle_no', '', ['placeholder' => 'Vehicle Number', 'class' => 'input-xlarge'])}}
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button class="btn btn-large btn-primary">Submit Now</button>
        </div>
    </div>
</fieldset>
</form>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
{{ HTML::script('js/bootstrap.min.js') }}
<script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
{{ HTML::script('js/scriptcam.js') }}
<script language="JavaScript">

    $(document).ready(function() {
        $('#door_number').keyup(function(){
            $('.name').fadeOut();
           if($('#door_number').val().length >= 3){
               $.ajax({
                   type: 'POST',
                   url: '{{ url('resident/ajaxnamesearch') }}',
                   data: 'bno=' + $('#block_number').val() + '&dno=' + $('#door_number').val(),
                   success: function(response){
                       $('#namelist').html('');
                       response = response.split(',');
                       var element = null;
                       for (var i = 0; i < response.length; i++) {
                           $('#namelist').html($('#namelist').html() + '<span class="name">' + response[i] + '</span>');
                       }
                       $('#namelist').fadeIn('slow');
                   }
               });
           }
        });

        $('#namelist').on('click', ".name", function(){
            $('.name').fadeOut();
            $(this).fadeIn();
            $('#tovisit').val($(this).html());
        });

    });
</script>
<script language="JavaScript">
    $(document).ready(function() {
        $("#webcam").scriptcam({
            showMicrophoneErrors:false,
            onError:onError,
            cornerRadius:20,
            cornerColor:'e3e5e2',
            onWebcamReady:onWebcamReady,
            onPictureAsBase64:base64_tofield_and_image,
            path:'{{ URL::to("") }}/js/'
        });
    });
    function base64_tofield() {
        $('#formfield').val($.scriptcam.getFrameAsBase64());
    };
    function base64_toimage() {
        $('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64()).slideDown();
    };
    function base64_tofield_and_image(b64) {
        $('#formfield').val(b64);
        $('#image').attr("src","data:image/png;base64,"+b64);
    };
    function changeCamera() {
        $.scriptcam.changeCamera($('#cameraNames').val());
    }
    function onError(errorId,errorMsg) {
        $( "#btn1" ).attr( "disabled", true );
        $( "#btn2" ).attr( "disabled", true );
        alert(errorMsg);
    }
    function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
        $.each(cameraNames, function(index, text) {
            $('#cameraNames').append( $('<option></option>').val(index).html(text) )
        });
        $('#cameraNames').val(camera);
    }
</script>

@stop