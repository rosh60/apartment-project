<?php

Route::group(array('before' => 'guard'), function()
{
	Route::get('tablet', 'Talk2rp\Tablet\Controllers\TabletController@getIndex');
	Route::post('tablet', 'Talk2rp\Tablet\Controllers\TabletController@postIndex');
});