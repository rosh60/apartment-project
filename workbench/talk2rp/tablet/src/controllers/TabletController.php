<?php namespace Talk2rp\Tablet\Controllers;

class TabletController extends \Controller
{

	public function getIndex()
	{
		return \View::make('tablet::visitor', ['title' => 'Visitors - Tablet']);
	}

	public function postIndex()
	{
        $grs = new \GrFingerService();
        if(!$grs->initialize()) return;
        
        $id = $grs->identify(\Input::get('tpt'));
        if($id == false)
            return \Response::json(['ret' => 0]);

        $person = \Resident::find($id);
        $person->visible = ['id', 'block_number', 'door_number', 'name', 'photo', 'car_front', 'car_back', 'bike_front', 'bike_back'];
        $person = $person->toArray();
        $person['photo'] = \URL::to('images/residents/'.$person['photo']);

        foreach(['car_front', 'car_back', 'bike_front', 'bike_back'] as $ele)
            if($person[$ele] != null)
                $person[$ele] = \URL::to('images/vehicles/'.$person[$ele]);

        $family = \Resident::where('block_number', $person['block_number'])
                            ->where('door_number', $person['door_number'])
                            ->where('id', '!=', $person['id'])
                            ->get()->toArray();

        foreach($family as $ele)
            $person['family'][] = \URL::to('images/residents/'.$ele['photo']);
        
        $grs->finalize();
        return \Response::json(['ret' => 1, 'data' => $person]);
	}

}