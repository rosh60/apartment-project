<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

// Event::listen('illuminate.query', function($sql){
// 	var_dump($sql);
// 	echo '<br>';
// });

Route::get('/', function(){
    if(Auth::check())
    {
        if(Auth::user()->role == 2)
            return Redirect::to('admin');
    }
	return View::make('layouts.home', ['title' => 'Welcome']);
});

Route::get('login', 'AuthController@getLogin');
Route::post('login', 'AuthController@postLogin');
Route::get('logout', 'AuthController@getLogout');
Route::post('api', 'ApiController@postMobileRequest');
Route::get('api/send', 'ApiController@getSend');
Route::post('api/send', 'ApiController@postSend');
Route::get('api/general', 'ApiController@getGeneral');
Route::post('api/general', 'ApiController@postGeneral');
Route::get('api/ajaxidsearch', 'ResidentController@ajaxGetIdByName');
Route::post('api/fingerservice/{name}', 'ApiController@getFingerService');

Route::group(array('before' => 'guard'), function()
{
	Route::Controller('guard', 'GuardController');
	Route::Controller('search', 'SearchController');
	Route::Controller('resident', 'ResidentController');
	Route::Controller('visitor', 'VisitorController');
});

Route::group(array('before' => 'resident'), function()
{
	Route::get('dashboard', 'DashboardController@showDashboard');
	Route::get('dashboard/profile', 'DashboardController@showProfile');
	Route::get('dashboard/profile/edit', 'DashboardController@getEditProfile');
	Route::post('dashboard/profile/edit', 'DashboardController@postEditProfile');
	Route::get('dashboard/profile/{id?}', 'DashboardController@showProfile');
	Route::get('complaints', 'ComplaintController@showMyComplaints');
	Route::get('complaint/register', 'ComplaintController@showComplaintForm');
	Route::post('complaint/register', 'ComplaintController@postComplaintForm');
	Route::get('market', 'MarketController@showMarket');
	Route::get('market/additem', 'MarketController@getAddItem');
	Route::post('market/additem', 'MarketController@postAddItem');
	Route::get('market/item/{id}', 'MarketController@showItem');
	Route::get('market/item/{id}/comments', 'MarketController@getComments');
	Route::post('market/item/{id}', 'MarketController@postComment');
	Route::resource('messages', 'MessageController');
	Route::controller('forum', 'ForumController');
});

Route::post('queue/receive', function()
{
    return Queue::marshal();
});