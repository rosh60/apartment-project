<?php

use Illuminate\Database\Migrations\Migration;

class CreateResidentStaffTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resident_staff', function($table){
			$table->increments('id');
			$table->text('resident_id', 11);
			$table->integer('staff_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resident_staff');
	}

}