<?php

use Illuminate\Database\Migrations\Migration;

class CreateResidentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('residents', function($table)
		{
		    $table->string('id', 10)->primary();
		    $table->string('block_number');
		    $table->integer('door_number');
		    $table->string('name', 50);
		    $table->string('dob', 10);
		    $table->string('gender', 6);
		    $table->bigInteger('mobile');
		    $table->string('occupation', 50);
		    $table->string('email', 100);
		    $table->integer('intercomm');
		    $table->string('user_status', 6);
		    $table->string('membership', 20);
		    $table->string('otp', 32);
		    $table->string('mid', 32);
		    $table->string('os_version', 10);
		    $table->string('device_type', 20);
		    $table->string('cpu_speed', 20);
		    $table->string('make_model', 50);
		    $table->integer('screen_height');
		    $table->integer('screen_width');
		    $table->string('gcm_token');
		    $table->string('blood_group', 20);
		    $table->string('identity_proof', 50);
		    $table->string('photo', 15);
		    $table->string('profile_photo', 15);
		    $table->float('longitude');
		    $table->float('latitude');
		    $table->string('status', 100);
		    $table->timestamps();
		    $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('residents');
	}

}