<?php

use Illuminate\Database\Migrations\Migration;

class CreateVisitorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visitors', function($table)
		{
			$table->increments('id');
			$table->string('block_number', 1);
			$table->integer('door_number');
			$table->string('visitor_name', 50);
			$table->string('person_to_visit', 50);
			$table->string('purpose', 50);
			$table->text('address');
			$table->bigInteger('mobile');
			$table->boolean('gender');
			$table->string('photo', 50);
			$table->dateTime('date');
			$table->string('vehicle_nos');
			$table->integer('guard_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visitors');
	}

}