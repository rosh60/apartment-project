<?php

use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff', function($table)
		{
			$table->increments('id');
			$table->string('name', 50);
			$table->string('category', 10);
			$table->string('gender', 6);
			$table->string('dob', 10);
			$table->bigInteger('mobile');
			$table->string('photo', 15);
			$table->text('address');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('staff');
	}

}