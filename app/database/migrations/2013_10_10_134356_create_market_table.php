<?php

use Illuminate\Database\Migrations\Migration;

class CreateMarketTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('market', function($table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->text('description');
			$table->string('picture', 50);
			$table->string('for_sale', 1);
			$table->string('for_rent', 1);
			$table->integer('price');
			$table->string('uid', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('market');
	}

}