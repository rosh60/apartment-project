<?php

use Illuminate\Database\Migrations\Migration;

class CreateGuardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guards', function($table)
		{
			$table->increments('id');
			$table->string('name', 50);
			$table->date('dob');
			$table->boolean('gender');
			$table->text('laddress');
			$table->text('paddress');
			$table->bigInteger('mobile');
			$table->string('origin', 50);
			$table->time('login');
			$table->time('logout');
			$table->string('position');
			$table->text('photo');
			$table->string('email', 100);
			$table->string('password', 60);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guards');
	}

}