<?php

use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vehicles', function($table){
			$table->increments('id');
			$table->string('slot', 10); 	// Parking slot number
			$table->integer('sticker'); 	// Sticker Number
			$table->string('model', 50); 	// Make or Model number 
			$table->string('number', 15); 	// Vehicle registration number
			$table->string('type', 10); 	// 2 Wheeler or 4 Wheeler
			$table->string('front', 15);	// Vehicle front photo
			$table->string('back', 15);		// Vehicle back photo
			$table->string('uid', 10); 		// Owner ID
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vehicles');
	}

}