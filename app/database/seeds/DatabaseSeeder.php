<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
	}

}


class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->insert([
        	[
        		'id' => 1,
                'email' => 'test@test.com',
                'password' => Hash::make('test'),
                'role' => 3
        	],
        	[
         		'id' => 2,
                'email' => 'admin@admin.com',
                'password' => Hash::make('test'),
                'role' => 2       	
        	]
        ]); 
    }
}