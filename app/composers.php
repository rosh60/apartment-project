<?php


View::composer(['layouts.residentsidebar'], function($view)
{
	$res = Resident::find(Auth::user()->id);
	$msgCount = $res->messages()->count();
	$cmpCount = $res->complaint()->count();
    $view->with(compact('msgCount', 'cmpCount'));
});
