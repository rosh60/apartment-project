<?php

class ComplaintController extends Controller{

	public function showComplaintForm()
	{
		return View::make('dashboard.addcomplaint', ['title' => 'Raise a complaint']);
	}

	public function postComplaintForm()
	{
		$cmp = new Complaint(Input::all());
		if($cmp->validateInput() == false)
		{
			return Redirect::to('complaint/register')->withErrors($cmp->v)->withInput();
		}
		$cmp->add();
		return Redirect::to('complaint/register')->with('message', 'Your complaint has been registered.');
	}

	public function showMyComplaints()
	{
		$complaints = Resident::find(Auth::user()->id)->complaint;
		return View::make('dashboard.showcomplaints', ['title' => 'My Complaints', 'data' => $complaints]);
	}

}