<?php

class MarketController extends Controller{

	public function showMarket()
	{
		$data = Market::orderBy('created_at', 'DESC')->get();
		$title = "Buy or Sell Items";
		return View::make('market.index', compact('title', 'data'));
	}

	public function getAddItem()
	{
		return View::make('market.additem', ['title' => 'Add an Item for Sale/Rent']);
	}

	public function postAddItem()
	{
		$input = Input::all();
		$v = Validator::make($input, Market::$rules);
		if($v->fails()) return Redirect::to('market/additem')->withErrors($v)->withInput();

		$filename = md5(time().rand(7, 777)).'.'.Input::file('picture')->getClientOriginalExtension();
		Input::file('picture')->move('images/useruploads/', $filename);
		$input['picture'] = $filename;
		$input['uid'] = Auth::user()->id;
		
		$item = new Market($input);
		$item->save();
		return Redirect::to('market/item/'.$item->id);
	}

	public function showItem()
	{
		$data = Market::with('resident')->find(Request::segment(3));
		$title = $data->name;
		return View::make('market.showitem', compact('title', 'data'));
	}

	public function getComments()
	{
		$result = Comment::getCommentsWithName();
		return $result;
	}

	public function postComment()
	{
		$cmt = new Comment();
		$cmt->comment = Common::sanitize(Input::get('reply'));
		$cmt->item_id = Request::segment(3);
		$cmt->uid = Auth::user()->id;
		$cmt->save();
		return Response::make('Success', 201);
	}

}