<?php

use Carbon\Carbon;

Class ForumController extends Controller
{

	public function getIndex()
	{
		$threads = Topic::orderBy('created_at', 'DESC')->get();
		return View::make('forum.index', compact('threads'));
	}

	public function getNewthread()
	{
		return View::make('forum.create');
	}

	public function postNewthread()
	{
		$input = Input::all();
		$v = Validator::make($input, Topic::$rules);
		if($v->fails()) return Redirect::to('forum/newthread')->withInput()->withErrors($v);

		$thread = new Topic();
		$thread->subject = Common::sanitize($input['subject']);
		$thread->uid = Auth::user()->id;
		$thread->save();

		$post = new Post();
		$post->content = Common::sanitize($input['content']);
		$post->uid = Auth::user()->id;
		$thread->posts()->save($post);

		return Redirect::to('forum');
	}

	public function getShowthread($id)
	{
		$thread = Topic::find($id);

		$dt = new Carbon;
		if($thread == null) return Redirect::to('forum');
		return View::make('forum.show', compact('thread', 'dt'));
	}

	public function postNewreply($id)
	{
		if(Input::get('post') == null) return Redirect::back()->withErrors('Please fill the post field');
		$post = new Post();
		$post->content = Common::sanitize(Input::get('post'));
		$post->uid = Auth::user()->id;
		$post->topic_id = $id;
		$post->save();
		return Redirect::back();
	}

	public function getDeletepost($id)
	{
		$post = Post::find($id);
		if($post->uid == Auth::user()->id) $post->delete();
		return Redirect::back();
	}

	public function getEditpost($id)
	{
		$post = Post::find($id);
		if($post->uid == Auth::user()->id)
			return View::make('forum.editpost', compact('post'));
	}

	public function postEditpost($id)
	{
		if(Input::get('post') == null) return Redirect::back()->withErrors('Please fill the post field');
		$post = Post::find($id);
		if($post->uid == Auth::user()->id)
		{
			$post->content = Common::sanitize(Input::get('post'));
			$post->update();
			return Redirect::to('forum');
		}
		return Redirect::to('forum');
	}

	public function getDeletethread($id)
	{
		$thread = Topic::find($id);
		if($thread->uid == Auth::user()->id) $thread->delete();
		return Redirect::back();
	}

}