<?php

class GuardController extends Controller{

	public function getAdd()
	{
		return View::make('guards.add', ['title' => 'Add New Guard']);
	}

	public function postAdd()
	{
		$guard = new Guard(Input::all());
		if($guard->validateInput() == false)
			return Redirect::to('guard/add')->withErrors($guard->v)->withInput();
		$guard->add();
		return Redirect::to('guard/add')->with('message', 'Added successfully.');
	}

	public function getAll()
	{
		$result = Guard::all();
		return View::make('guards.list', ['title' => 'Guards List', 'data' => $result]);
	}

}