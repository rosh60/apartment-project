<?php

class AuthController extends Controller
{

	public function getLogin()
	{
		return View::make('login.login')->with('title', 'Login');
	}

	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('/');
	}

	public function postLogin()
	{
		$input = Input::all();
		$v = Validator::make($input, ['email' => 'required|email', 'password' => 'required']);
		if($v->fails())
		{
			return Redirect::to('login')->withErrors($v);
		}
		
		if(Auth::attempt(['email' => $input['email'], 'password' => $input['password']], $input['remember']))
		{
			// Below 2 lines are a temporary fix. Remove it in final stages.
			if(Auth::user()->role == 1) return Redirect::to('dashboard');
			if(Auth::user()->role == 2) return Redirect::to('admin');

			Auth::user()->touch();
			return Redirect::intended('/');
		}
		return Redirect::to('login')->with('message', 'Invalid username or password.');
	}

}