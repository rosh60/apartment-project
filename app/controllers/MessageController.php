<?php

class MessageController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = Message::where('to', Auth::user()->id)->with('sender')->get();
		return View::make('messages.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('messages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$v = Validator::make($input, Message::$rules);
		if($v->fails())
		{
			return Response::make($v->messages()->first(), 200);
		}
		$input['from'] = Auth::user()->id;
		$m = new Message(Common::sanitize($input));
		$m->save();
		return Response::make('success', 201);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if(!is_numeric($id)) return Redirect::to('messages')->with('message', 'Invalid ID.');
		$msg = Message::find($id);
		if(is_null($msg)) return Redirect::to('messages')->with('message', 'Invalid ID.');
		if (!(($msg->from == Auth::user()->id) || ($msg->to == Auth::user()->id))) return Redirect::to('messages')->with('message', 'Invalid ID.');
		return View::make('messages.show', compact('msg'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Message::find($id)->delete();
		return Redirect::to('messages')->with('message', 'Message has been deleted.');
	}
	
}