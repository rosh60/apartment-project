<?php

use Carbon\Carbon;

class VisitorController extends Controller{

	public $v;

	public function getAdd()
	{
		return View::make('visitors.add', ['title' => 'Add New Visitor']);
	}

	public function postAdd()
	{
		$visitor = new Visitor(Input::all());
		if($visitor->validateInput() == false)
		{
			return Redirect::to('visitor/add')->withErrors($visitor->v)->withInput();
		}
		$visitor->add();
		return Redirect::to('visitor/add')->with('message', 'Added successfully.');
	}

	public function getAll()
	{
		$result = Visitor::orderBy('date', 'DESC')->get();
		return View::make('visitors.list', ['title' => 'Visitors List', 'data' => $result]);
	}

	public function getReport()
	{
		return View::make('visitors.report', ['title' => 'Generate Reports']);
	}

	public function postReport()
	{
		$v = new Visitor();
		$data = $v->getReportData(Common::sanitize($_POST['visitor_report']));
		if(!$data) return Redirect::to('visitor/report');
		$param = $_POST['visitor_report'];
        $title = 'Visitor Reports';
        return View::make('visitors.list', compact('title', 'data', 'param'));
	}

	public function getReportDownload($param)
	{
		$v = new Visitor();
		$data = $v->getReportData(Common::sanitize($param));
		if(!$data) return Redirect::to('visitor/report');
		$pdf = PDF::loadView('other.pdfreport', compact('data'));
		return $pdf->download('report.pdf');
	}

}