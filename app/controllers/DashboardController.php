<?php

class DashboardController extends Controller{

	public function showDashboard()
	{
		$data = $this->collectDashboardData();
		return View::make('dashboard.index', compact('data'));
	}

	private function collectDashboardData()
	{
		$data = array();
		$res = Resident::find(Auth::user()->id);

		$data['name'] = $res->name;

		if($count = $res->messages()->count())
		{
			$data['msgs'] = [
							'count' => $count,
							'from' => Resident::find($res->messages()->first()->from)->name
			];
		}
		else $data['msgs'] = ['count' => 0];

		$data['cmps'] = $res->complaint()->count();

		return $data;
	}

	public function showProfile($id = null)
	{
		$data = Resident::find(($id == null) ? Auth::user()->id : $id);
		return View::make('dashboard.profile', compact('data'));
	}

	public function getEditProfile()
	{
		$id = Auth::user()->id;
		$res = Resident::find($id)->toArray();
		$res['messages'] = Message::where('to', Auth::user()->id)->count();
		$res['gender'] = $res['gender'] ? 'Male' : 'Female';
		Session::flashInput($res);
		return View::make('dashboard.editprofile');
	}

	public function postEditProfile()
	{
		$input = Input::all();
		$id = Auth::user()->id;
		$res = Resident::find($id);
		$v = Validator::make($input, [
					'profile_photo' => 'mimes:jpeg',
					'car_front' => 'mimes:jpeg',
					'car_back' => 'mimes:jpeg',
					'bike_front' => 'mimes:jpeg',
					'bike_back' => 'mimes:jpeg',
					'mobile' => 'required|numeric',
					'email' => 'required'
				]);
		if($v->fails()) return Redirect::to('dashboard/profile/edit')->withErrors($v);

		if(Input::hasFile('profile_photo'))
		{
			$filename = $id.'.jpg';
			Input::file('profile_photo')->move(public_path('images/residents/profile'), $filename);
			$res->profile_photo = $filename;
		}

		if(Input::hasFile('car_front'))
		{
			$filename = Str::random(11).'.jpg';
			Input::file('car_front')->move(public_path('images/vehicles'), $filename);
			$res->car_front = $filename;
		}

		if(Input::hasFile('car_back'))
		{
			$filename = Str::random(11).'.jpg';
			Input::file('car_back')->move(public_path('images/vehicles'), $filename);
			$res->car_back = $filename;
		}

		if(Input::hasFile('bike_front'))
		{
			$filename = Str::random(11).'.jpg';
			Input::file('bike_front')->move(public_path('images/vehicles'), $filename);
			$res->bike_front = $filename;
		}

		if(Input::hasFile('bike_back'))
		{
			$filename = Str::random(11).'.jpg';
			Input::file('bike_back')->move(public_path('images/vehicles'), $filename);
			$res->bike_back = $filename;
		}

		$res->occupation = Common::sanitize($input['occupation']);
		$res->email = Common::sanitize($input['email']);
		$res->mobile = Common::sanitize($input['mobile']);
		$res->status = Common::sanitize($input['status']);
		$res->update();
		return Redirect::to('dashboard/profile/edit')->with('message', 'Updated.');
	}

}