<?php

class SearchController extends Controller{

	/**
	 * Shows Search Form
	 */
	public function getIndex()
	{
		return View::make('search.search', ['title' => 'Search']);
	}

	/**
	 * Handles ajax based search for autopopulating login form
	 */
	public function postIndex()
	{
		$key = Common::sanitize(Input::get('search'));
		$result = DB::select("
		SELECT DISTINCT name FROM residents WHERE name LIKE '%$key%'
		UNION
		SELECT DISTINCT visitor_name FROM visitors WHERE visitor_name LIKE '%$key%'
		UNION
		SELECT DISTINCT name FROM guards WHERE name LIKE '%$key%'
		UNION
		SELECT DISTINCT blood_group FROM residents WHERE blood_group LIKE '%$key%'
		");
		return $this->getArrayValues($result);
	}

	public function postResults()
	{
		$key = Common::sanitize(Input::get('search'));
		$blood_groups = ['A Positive', 'A Negetive', 'B Positive', 'B Negetive', 'AB Positive', 'AB Negetive', 'O Positive', 'O Negetive'];
		if(in_array($key, $blood_groups))
		{
			$result = Resident::where('blood_group', $key)->get();
			return View::make('search.results', ['title' => 'Search Results', 'data' => $result]);
		}
		$result = Resident::where('name', 'like', "%$key%")->get();
		return View::make('search.results', ['title' => 'Search Results', 'data' => $result]);
	}

	public function postVisitorSearch()
	{
		$key = Common::sanitize(Input::get('visitor_search'));
		$from = Common::sanitize(Input::get('date_from'));
		$to = Common::sanitize(Input::get('date_to'));
		$title = 'Visitor Search Results';
		if(!empty($from) || !empty($to))
		{
			$data = DB::select("SELECT * FROM visitors WHERE DATE_FORMAT(DATE, '%d-%m-%Y') BETWEEN ? AND ?", [$from, $to]);
			return View::make('visitors.list', compact('title', 'data'));
		}
		if(strpos($key, ',') !== FALSE)
		{
			$key = explode(',', $key);
			$key[1] = trim($key[1]);
			$data = DB::select("SELECT * FROM visitors WHERE visitor_name LIKE '%$key[1]%' AND CONCAT(block_number, door_number) LIKE '%$key[0]%'");
			return View::make('visitors.list', compact('title', 'data'));
		}
		$data = DB::select("SELECT * FROM visitors WHERE visitor_name LIKE '%$key%' OR mobile LIKE'%$key%' OR CONCAT(block_number, door_number) LIKE '%$key%'");
		return View::make('visitors.list', compact('title', 'data'));
	}

	private function getArrayValues($old)
	{
		$new = array();
		foreach($old as $ele){
			$new[] = $ele->name;
			if(isset($ele->blood_group)) $new[] = $ele->blood_group;
		}
		return $new;
	}
}