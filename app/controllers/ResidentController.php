<?php

class ResidentController extends Controller
{

	// Temporary verification functions //
	
	public function getVerify()
	{
		return View::make('other.verify', ['title' => 'Verification']);
	}

	public function postVerify()
	{
		return Input::all();
	}

	// Temporary verification functions //


	public function getAdd()
	{
		return View::make('residents.add', ['title' => 'Add New Resident']);
	}

	public function postAdd()
	{
		$resident = new Resident(Input::all());
		if($resident->validateInput() == false)
		{
			return Redirect::to('resident/add')->withErrors($resident->v)->withInput();
		}
		$resident->add();
		return Redirect::to('resident/add')->with('message', 'Added successfully.');
	}

	public function getAll()
	{
		$resident = new Resident();
		$result = $resident->listAll();
		return View::make('residents.list', ['title' => 'Resident List', 'data' => $result]);
	}

	public function postAjaxnamesearch()
	{
		$result = DB::table('residents')->select('name')->where('block_number', Input::get('bno'))->where('door_number', Input::get('dno'))->get();
		$new = '';
        foreach($result as $ele){
            $new .= $ele->name.',';
        }
        return rtrim($new, ',');
	}

	public function ajaxGetIdByName()
	{
		$key = Common::sanitize(Input::get('search'));
		return Resident::getIdByName($key);
	}

}