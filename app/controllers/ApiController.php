<?php

use Carbon;

class ApiController extends Controller{

	public function getSend()
	{
		return '<form action="send" method="POST" >
    <input type="text" name="gcm_token" placeholder="Enter GCM Token Here" /><br />
    <input type="text" name="type" placeholder="Request Type (General/Personal)" /><br />
    <input type="text" name="visitor_name" placeholder="Enter Name of visitor here" /><br />
    <input type="text" name="visitor_pic" placeholder="Link to visitor\'s picture" /><br />
    <input type="text" name="contact_num" placeholder="Contact number goes here" /><br />
    <input type="text" name="purpose" placeholder="Purpose of visit" /><br />
    <input type="text" name="to_meet" placeholder="To meet" /><br />
    <input type="submit" value="Send Notification" />
</form>';
	}

	public function postSend()
	{
	    $input = Input::all();
        $message = array('message' => array(
            'type' => $input['type'],
            'visitor_name' => $input['visitor_name'],
            'visitor_pic' => $input['visitor_pic'],
            'contact_num' => $input['contact_num'],
            'purpose' => $input['purpose'],
            'to_meet' => $input['to_meet'],
        ));
        $registatoin_ids = array($input['gcm_token']);
        $data['registration_ids'] = $registatoin_ids;
        $data['message'] = $message;

        $result = Common::sendNotification($data);
        echo $result;
	}

	public function getGeneral()
	{
		return '<form action="general" method="POST" >
	    <input type="text" name="gcm_token" placeholder="Enter GCM Tokens Here(Comma Seperate)" /><br />
	    <input type="text" name="type" placeholder="Request Type (General/Personal)" /><br />
	    <input type="text" name="title" placeholder="Title" /><br />
	    <input type="text" name="description" placeholder="Description" /><br />
	    <input type="submit" value="Send Notification" />
</form>';
	}

	public function postGeneral()
	{
	    $input = Input::all();
	    if (strpos($input['gcm_token'], ',') !== false)
		  $registatoin_ids = explode(',', $input['gcm_token']);
		else
			$registatoin_ids = array($input['gcm_token']);
        $message = array('message' => array(
            'type' => $input['type'],
            'title' => $input['title'],
            'description' => $input['description']
        ));
        $data['registration_ids'] = $registatoin_ids;
        $data['message'] = $message;

        $result = Common::sendNotification($data);
        echo $result;
	}


	public function dupes()
	{	exit;
		for($i=160; $i<500; $i++)
		{
			$id = substr(md5($i), 0, 10);
			$usr = new User(['id' => $id, 'email' => $id, 'password' => $id, 'role' => 1, 'fingerprint' => 'p/8BHZQA/AACfwCcAPYAAjQBWQC/AAFOAKkATQACVgGNAEQAAcYAlgA6AAIHALkApQACOAG6AJIAAjgBjQAQAQKEANYABgEBRwHRAOwAAjoBpwAUAQFFAacAxAACdwBwAAkBAjsBigDdAAIhAdkAVgACfwDQADcAAjsBxgBwAAI0AYgAmgAC5ACgAMMAAWwA3gCqAAIvAegAigACKwFnAIAAAj4AYwBcAAItAI8ApQACOQCmALgAAnkAUwDWAAJPAFsA8wACLgFWAOAAAmIAnwAMExwaAAEMGRMZBAUYEgYHGxwIABoCCwgJCgMFGQYDBAgBCA0ZGBsaDRsLAAEOCwERDw8QHAIADhMYFBUOEwcRFhcMBhQGDQAODAwYEwYZEhIWBhgUBxkHFREDEBEDDhkYBwcVExIXBA0BCwkNHA8DBhIBEw0OCA4BDBIHGw4MEhwOGwIMBxUPGBYGFQEKBhETBw4aCw0OGA4CCBsAGwsKABMREAoMGRQFEA0aAAwCEgIYCQEXBQwUCw4UEQAKChMBGwIWExQKGQoUCQAOEgcPBBAAHBMCBwMWBBcDEQUSFxEECBwBHAoODwUICQAaCgYSERoYDwQUGBoTGRULDAkMFgMUDxkWEgMWBQcWGBcVEBIECRMJDgwVFQMaFhgDCRQHEAcFAhcBFAoVERcCBwYQCQcLFBoXCRUCBA4XAgM=']);
			$usr->save();
			$id = substr(md5($i . $i . time()), 0, 10);
			$usr = new User(['id' => $id, 'email' => $id, 'password' => $id, 'role' => 1, 'fingerprint' => 'p/8BFmgATAABYAFfAEEAArEAdADxAAJ8AHkA3QACeQBzAJMAAlYBjQCRAAGTAHAArAABWwFOAFYAAb0AWgCdAAHIAEkA2wABXQAyAPIAAV0AygAYAQJBAcgA9wACOgEpAM8AAVUAmAByAAFHAZ4A6QACOgG6AM8AAoIAHQDBAAJMAMoAvAABhABwAMkAAnEAaQD2AAIrAR0AmQABPgB4ABQCAAENEQIDAxMQEgYEBAUHAQgEBggHABMGFAMLDAUOCgkJDQoNDxAPAxEVBgUCEwwQFAkCDwkTDA8UEwMJAgkTCAQOAwYJEQgFChETBA0VFAoUDw8TDBIOAAgVCQYTBQ8SCQgLDwMQCgICBg0IBg4RCAQHDRMDCAQACAcQEwMECgMKExQGCxAOARIFFA0IDg8GEAUFAAIQDgcJFQ0GCAAQBgwDFQcTEQwCBAEGFQUHFQQRBg8JAxISDhIGChUTEgsSBgcFAQgBEAQUEQoICwIMFAYAEgQQDgwTCwMLFBUAEAgGARUBDAURBwsTCwkLCg==']);
			$usr->save();
			$id = substr(md5($i . time()), 0, 10);
			$usr = new User(['id' => $id, 'email' => $id, 'password' => $id, 'role' => 1, 'fingerprint' => 'p/8BHtIAeQABNgHeAHQAAYIAkgAdAAFgAdIA7QACTAHjAOgAAUcBxwDWAAE7Ac4AyAACNgHAAHkAAoIArwBzAAI5AZMAmQACVwCMAIUAAhEAxwCvAAJsAMgAmAACdABPANsAAV0AUAC8AAJaAJ8A6AABmQCOAEoAAmQBgACwAAJdAJ0AoQABZgBVAHsAAUIAdABuAALcALIABAEBWAGyAEYAApMArgDJAAE/AUQAfwAC+wCbAJIAAm4AfADpAAJ/AD8AoQABTwClALUAARQBeQDyAAI5AakAHRoJGQABEgkSGQUGGBMHAAcIAwQZCgkKFxwcEgsMAwUGCwUXBwERCQ0ODAcOGxcGDAAcCQoUERIEBRIKDxcVDxsYExQaDxwLEBYACBcLHBkDBhwRGQgEBh0PCggFCxUDERkMARcSFBALEhIMDwUbEwYcGQcRChwMEAIMGQgWDAgaDQkIAQgFHB0NBgwOEQMPFQUDFxgUDxwXERYCEgcZFAkUCgcJDAcWCBALGRwKCwcLAAoTFQQaEQ8GChAIFBoXFRcVGg0bFR0AFhkABBcOGBoOAwsLAQQLARYPEQ4TDRESFBUGGhwSAB0RHRcRGxEUDwQdDhETBxAJExELAxwKGBkQGQEZEwoWFBYbFB0cExAOCREYEgEZFgQcDw0FERsKCRgbCQwWBgEUAhkYHQMBEA8OCAIYEA0YGhsNHBcBBQEHAgoCExYAAhMCBAEBAhgCGwI=']);
			$usr->save();
		}
	}

	public function getFingerService($name)
	{
		$grs = new GrFingerService();
		// Calling the application startup code
		if($grs->initialize())
		{
			// Posting the template to be identified		
			$id = $grs->identify(Input::get('tpt'));
			// Returning the result of the identification process
			if($id == false)
				return ['ret' => 0];
			else
			{
				$person = ['id' => $id, 'name' => Resident::find($id)->name]; 
				return $person;
			}
			// Calling the finalization code
			$grs->finalize();
		}
	}

	public function postMobileRequest()
	{
		$input = Input::all();
		switch ($input['method_name']) {
			case 'authentication':
				return $this->registerMobile($input);
				break;

			case 'profile_pic' :
				return $this->uploadProfilePic($input);
				break;

			case 'match' :

					file_put_contents("file.txt", Input::get('template'));
					$grs = new GrFingerService();
					// Calling the application startup code
					if($grs->initialize())
					{
						// Posting the template to be identified		
						$id = $grs->identify($input['template']);
						// Returning the result of the identification process
						if($id == false)
							return 'failure';
							//return ['ret' => 0];
						else
						{
							//$person = ['id' => $id, 'name' => Resident::find($id)->name]; 
							return 'success';
							return $person;
						}
						// Calling the finalization code
						$grs->finalize();
					}
				break;
			
			default:
				return Response::make('Method not found.', 404);
				break;
		}
	}
	
	private function registerMobile($input)
	{
		$res = Resident::where('otp', $input['otp'])->first();
		if(is_null($res)) return Response::make('Authentication failed', 401);
		if($res->block_number.'-'.$res->door_number == $input['block_door'])
		{
			$input['mid'] = md5($input['imei']);
			$res->update($input);
			$res = $res->toArray();
			$res['photo'] = URL::to('images/residents').'/'.$res['photo'];
			if($res['profile_photo'] != null) $res['profile_photo'] = URL::to('images/residents/profile').'/'.$res['profile_photo'];
			return Response::make(array_except($res, ['id', 'vehicle_nos', 'otp', 'os_version', 'device_type', 'cpu_speed', 'make_model', 'screen_height', 'screen_width', 'gcm_token', 'longitude', 'latitude', 'created_at', 'updated_at']), 202);
		}
		return Response::make('Authentication failed', 401);
	}

	private function uploadProfilePic($input)
	{
		$res = Resident::where('mid', $input['unique_id'])->first();
		if(is_null($res)) return Response::make('Authentication failed', 401);
		file_put_contents(public_path().'/images/residents/'.$res->id.'.jpg', base64_decode($input['photo']));
		$res->photo = $res->id.'.jpg';
		$res->update($input);
		return Response::make('Success', 202);
	}

}