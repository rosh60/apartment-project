@extends('layouts.master')

@section('content')

{{ Form::open(['url' => 'visitor/report', 'method' => 'post', 'class' => 'form-horizontal'])}}
    <div class="control-group">
        <label class="control-label"><h2>Generate Visitor Report</h2></label>
        <div class="controls">
            <label class="radio">
                <input type="radio" name="visitor_report" value="today" />
                Today's Visitor Report
            </label>
            <label class="radio">
                <input type="radio" name="visitor_report" value="guard_today" />
                Guard shift visitor report
            </label>
            <label class="radio">
                <input type="radio" name="visitor_report" value="all_visitors" />
                All visitors report
            </label>
        </div>
        <div class="control-group">
            <div class="controls">
                <button class="btn btn-large btn-primary">Generate Report</button>
            </div>
        </div>
    </div>

</form>

@stop