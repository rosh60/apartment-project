<?php use Carbon\Carbon ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Visitor Registration - PC</title>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
        <style>
        #namelist{
            width:150px;
            display: inline-block;
            position: absolute;
            top:0;
            left:200px;
            margin:-2px 0 0 8px;
        }
        .name{
            padding:5px 8px;
            background: #AB00FF;
            border-radius:3px;
            color:white;
            cursor:pointer;
            margin:2px 5px 2px 0;
            display:inline-block;
        }
        .name:hover{
            background:#0000FF;
        }
    </style>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    {{ HTML::script('js/pcbio.js') }}
</head>
<body onbeforeunload="Finalize()">
<OBJECT ID="GrFingerX" CLASSID="CLSID:71944DD6-B5D2-4558-AD02-0435CB2B39DF" style="display:none;"></OBJECT>
{{ HTML::script('js/GrFinger.js') }}

<SCRIPT FOR="GrFingerX" EVENT="SensorPlug(id)" LANGUAGE="javascript">
GrFingerX.CapStartCapture(id);
document.getElementById('log').value ="";
document.getElementById('log').value = document.getElementById('log').value + "Initialized\n";
</SCRIPT>

<script type="text/javascript">
var i = 0;
</script>

<SCRIPT FOR="GrFingerX" EVENT="SensorUnplug(id)" LANGUAGE="javascript">
GrFingerX.CapStopCapture(id);
document.getElementById('log').value = document.getElementById('log').value + "Stopped\n";
</SCRIPT>

<SCRIPT FOR="GrFingerX" EVENT="FingerDown(id)" LANGUAGE="javascript">
document.getElementById('log').value = "Fingerprint Captured\n";
</SCRIPT>

<SCRIPT FOR="GrFingerX" EVENT="FingerUp(id)" LANGUAGE="javascript">
document.getElementById('log').value = document.getElementById('log').value + "\n";
</SCRIPT>

<SCRIPT FOR="GrFingerX" EVENT="ImageAcquired(id, w, h, rawImg, res)" LANGUAGE="javascript">
GrFingerX.CapSaveRawImageToFile(rawImg, w, h, "D:\\teste.bmp", 501);
Start();
if(document.getElementById('img').style.display == 'none')
    document.getElementById('img').style.display = 'block';
base64_tofield();
base64_toimage();
CallEnroll(rawImg, w, h, res);
</SCRIPT>

<SCRIPT language="JavaScript" type="text/javascript">
<!--
function Start() {
document.getElementById("img").src = "D:\\teste.bmp?ts" + encodeURIComponent( new Date().toString() );
}
</SCRIPT> 

    <div id="container" class="table-bordered">
        <div class="col-md-3" style="width:360px;">
            <div class="thumbnail"><div id="webcam"></div>
            <img id="campic" />
            <span class="glyphicon glyphicon-user"></span> Name : Roshan Pal<br/>
            <span class="glyphicon glyphicon-info-sign"></span> Door & Block # : A-111
            </div>
            <div><img id="img" class="thumbnail"  src="http://www.generatorstudios.com/fingerprint.gif" /></div>
            
            <div class="list-group">
                <a href="#" class="list-group-item">
                <p class="list-group-item-text">Uma Mageshwari (11:49 AM) # A-111</p>
                </a>
                <a href="#" class="list-group-item">
                <p class="list-group-item-text">Mohanlal (2:20 PM) # A-111</p>
                </a>
                <a href="#" class="list-group-item">
                <p class="list-group-item-text">Kumar sanu (3:49 PM) # A-111</p>
                </a>
            </div>
        </div>
  <div class="col-md-5 table-bordered" style="width:650px;">
    {{ Form::open(['url' => 'visitor/add', 'method' => 'post']) }}
    @if($errors->any())
    {{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
    @endif
    @if($message = Session::get('message'))
    {{ '<div class="alert alert-success">'.$message.'</div>' }}
    @endif

    {{ Form::hidden('fingerprint', '', ['id' => 'fingerprint'])}}
    {{ Form::hidden('photo', '', ['id' => 'photo'])}}   
<fieldset>

        <label class="control-label">Block & Door No.<span style="color:red">*</span></label>
    <div class="row col-md-12">

        <div class="col-xs-3 col-md-3">
            {{ Form::select('block_number', [
            '' => 'Block',
            'A' => 'A',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D',
            'E' => 'E',
            'F' => 'F'
            ], 'Block', ['id' => 'block_number', 'class' => 'form-control', 'required' => '']) }}
        </div>
        <div class="col-xs-4 col-md-4">
            {{ Form::text('door_number', '', ['id' => 'door_number', 'required' => '', 'maxlength' => '3', 'placeholder' => 'Door Number', 'class' => 'form-control', 'autocomplete' => 'off']) }}<div id="namelist"><span class="name">Person to visit ?</span></div>
        </div>
    </div>
    <div class="control-group">
<!--        <label class="control-label">Person to visit<span style="color:red">*</span></label>-->
        <div class="controls">
            <div id="namelist" style="display: none;"></div>
            {{ Form::hidden('person_to_visit', '', ['id' => 'tovisit'])}}
            <p class="help-block"></p>
        </div>
    </div>
<div class="col-md-9">
    <div class="control-group">
        <label class="control-label">Visitor Name<span style="color:red">*</span></label>
        <div class="controls">
            {{ Form::text('visitor_name', '', ['required' => '', 'placeholder' => 'Full Name', 'class' => 'form-control'])}}
            <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Purpose of visit<span style="color:red">*</span></label>
        <div class="controls">
            {{ Form::text('purpose', '', ['required' => '', 'placeholder' => 'Purpose of Visit', 'class' => 'form-control'])}}
            <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Address / Coming From<span style="color:red">*</span></label>
        <div class="controls">
            {{ Form::text('address', '', ['required' => '', 'placeholder' => 'Address / Coming From', 'class' => 'form-control'])}}
            <p class="help-block">Street address, P.O. box, company name, c/o</p>
        </div>
    </div>
    <!-- city input-->
    <div class="control-group">
        <label class="control-label">Mobile<span style="color:red">*</span></label>
        <div class="controls">
            {{ Form::text('mobile', '', ['required' => '', 'placeholder' => 'Mobile Number', 'class' => 'form-control']) }}
            <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Gender</label>
        <div class="controls">
            <label class="radio inline">
                {{ Form::radio('gender', '1') }}
                Male
            </label>
            <label class="radio inline">
                {{ Form::radio('gender', '0') }}
                Female
            </label>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Vehicle Number</label>
        <div class="controls">
        {{ Form::text('vehicle_no', '', ['placeholder' => 'Vehicle Number', 'class' => 'form-control'])}}
        <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button class="btn btn-large btn-primary">Submit Now</button>
        </div>
    </div>
</div>
</fieldset>
</form>





        </div>
        <div class="col-lg-3">
            <div class="thumbnail"><span class="glyphicon glyphicon-user"></span>
                Guard Name : **Requires Admin Section<br/>
            <span class="glyphicon glyphicon-time"></span>
                Time logged in : {{ Carbon::parse(Auth::user()->updated_at)->diffInMinutes() }} Minutes</div>
        </div>
        <textarea id="log"></textarea>
    <div class="clearfix"></div>
    </div>


{{ HTML::script('js/bootstrap.min.js') }}
<script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
{{ HTML::script('js/scriptcam.js') }}
<script type="text/javascript">
$(document).ready(function() {
    Initialize();
    var i = 0;
    $('#door_number').keyup(function(){
        $('.name').fadeOut();
       if($('#door_number').val().length >= 3){
           $.ajax({
               type: 'POST',
               url: '{{ url('resident/ajaxnamesearch') }}',
               data: 'bno=' + $('#block_number').val() + '&dno=' + $('#door_number').val(),
               success: function(response){
                   $('#namelist').html('');
                   response = response.split(',');
                   var element = null;
                   for (var i = 0; i < response.length; i++) {
                       $('#namelist').html($('#namelist').html() + '<span class="name">' + response[i] + '</span>');
                   }
                   $('#namelist').fadeIn('slow');
               }
           });
       }
    });

    $('#namelist').on('click', ".name", function(){
        $('.name').fadeOut();
        $(this).fadeIn();
        $('#tovisit').val($(this).html());
    });

});

$(function() {
    $("#webcam").scriptcam({
        showMicrophoneErrors:false,
        onError:onError,
        cornerRadius:20,
        cornerColor:'ffffff',
        onWebcamReady:onWebcamReady,
        onPictureAsBase64:base64_tofield_and_image,
        path:'{{ URL::to("") }}/js/'
    });
});
function base64_tofield() {
    $('#photo').val($.scriptcam.getFrameAsBase64());
};
function base64_toimage() {
    
    $('#campic').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64());
    $('#webcam').hide();
};
function base64_tofield_and_image(b64) {
    $('#formfield').val(b64);
    $('#image').attr("src","data:image/png;base64,"+b64);
};
function changeCamera() {
    $.scriptcam.changeCamera($('#cameraNames').val());
}
function onError(errorId,errorMsg) {
    $( "#btn1" ).attr( "disabled", true );
    $( "#btn2" ).attr( "disabled", true );
    alert(errorMsg);
}
function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
    $.each(cameraNames, function(index, text) {
        $('#cameraNames').append( $('<option></option>').val(index).html(text) )
    });
    $('#cameraNames').val(camera);
}
</script>
</body>
</html>