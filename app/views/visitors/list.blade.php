@extends('layouts.master')

@section('content')

<div class="list">
<table class="table table-bordered">
    <tr>
        <td>Block & Door No.</td>
        <td>Name</td>
        <td>Person to visit</td>
        <td>Purpose</td>
        <td>Mobile</td>
        <td>Address</td>
        <td>Vehicle Number</td>
        <td>Date</td>
        <td>Photo</td>
    </tr>
    <?php foreach($data as $row): ?>
        <tr>
            <td><?=$row->block_number.$row->door_number?></td>
            <td><?=$row->visitor_name?></td>
            <td><?=$row->person_to_visit?></td>
            <td><?=$row->purpose?></td>
            <td><?=$row->mobile?></td>
            <td><?=$row->address?></td>
            <td><?=$row->vehicle_nos?></td>
            <td><?=date('d-m-Y', strtotime($row->date))?></td>
            <td><img src="{{ url('images/visitors/'.$row->photo) }}" width="20%" /></td>
        </tr>
    <?php endforeach; ?>
</table>
@if(isset($param))
<a href="{{ url('visitor/report-download/'.$param) }}" class="btn btn-primary">Download Report</a>
@else
<a href="{{ url('visitor/report-download/all') }}" class="btn btn-primary">Download Report</a>
@endif
</div>

@stop