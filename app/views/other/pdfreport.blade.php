<!DOCTYPE html>
<html>
<head>
    {{ HTML::style('css/main.css') }}
    {{ HTML::style('css/visitor.css') }}
</head>
<body>
<table class="table table-stripped">
    <tr>
        <td>Block & Door No.</td>
        <td>Name</td>
        <td>Person to visit</td>
        <td>Purpose</td>
        <td>Mobile</td>
        <td>Address</td>
        <td>Vehicle Number</td>
        <td>Date</td>
    </tr>
    <?php foreach($data as $row): ?>
        <tr>
            <td><?=$row->block_number.$row->door_number?></td>
            <td><?=$row->visitor_name?></td>
            <td><?=$row->person_to_visit?></td>
            <td><?=$row->purpose?></td>
            <td><?=$row->mobile?></td>
            <td><?=$row->address?></td>
            <td><?=$row->vehicle_nos?></td>
            <td><?=date('d-m-Y', strtotime($row->date))?></td>
        </tr>
    <?php endforeach; ?>
</table>
<footer></footer>
</body>
</html>