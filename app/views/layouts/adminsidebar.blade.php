<div class="list-group">
  <a href="{{ url('admin/residents') }}" class="list-group-item sidenav">Manage Users</a>
  <a href="{{ url('admin/staff') }}" class="list-group-item sidenav">Manage Staff</a>
  <a href="{{ url('admin/membership-committee') }}" class="list-group-item sidenav">Manage MC</a>
  <a href="{{ url('admin/visitors') }}" class="list-group-item sidenav">Manage Visitors</a>
  <a href="{{ url('admin/complaints') }}" class="list-group-item sidenav">Manage Complaints</a>
  <a href="{{ url('admin/residents/history') }}" class="list-group-item sidenav">Resident History</a>
  <a href="{{ url('admin/residents/vehicle') }}" class="list-group-item sidenav">Manage Vehicles</a>
</div>