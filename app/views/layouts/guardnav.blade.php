<div class="navbar">
    <div class="navbar-inner">
        <div class="span12 centerdiv">
            <ul class="nav">
                <li><a href="{{ url('') }}">Home</a></li>
                <li><a href="{{ url('visitor/add') }}">Visitor</a></li>
                <li><a href="{{ url('resident/add') }}">Resident</a></li>
                <li><a href="{{ url('visitor/all') }}">Visitor List</a></li>
                <li><a href="{{ url('resident/all') }}">Resident List</a></li>
                <li><a href="{{ url('visitor/report') }}">Reports</a></li>
                <li><a href="{{ url('search') }}">Search</a></li>
                <li><a href="{{ url('resident/verify') }}">Verify</a></li>
                @if(Auth::check())
                    <li><a href="{{ url('logout') }}">Logout</a></li>
                @else
                    <li><a href="{{ url('login') }}">Login</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>