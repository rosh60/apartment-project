<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>@yield('title')</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
        <style>
          #namedrop{
            position:absolute;
            width:35%;
            background:white;
            border:1px solid #ccc;
            left:27%;
            display:none;
          }
          .element{
              padding:5px 10px;
              display: block;
              font-size: 16px;
          }
          .element:hover{
              background-color:#a566e7;
              color:white;
          }
          .labelLike{
            border:0;
            cursor: pointer;
          }
          .labelLike:focus{
            border:1px solid #ccc;
            cursor: text;
            padding: 3px 10px;
            border-radius: 5px;
          }
        </style>
    </head>
    <body style="width:95%; margin:0 auto; padding-top: 70px;" onbeforeunload="Finalize()">
    <OBJECT ID="GrFingerX" CLASSID="CLSID:71944DD6-B5D2-4558-AD02-0435CB2B39DF" style="display:none;"></OBJECT>
    {{ HTML::script('js/GrFinger.js') }}

    <SCRIPT FOR="GrFingerX" EVENT="SensorPlug(id)" LANGUAGE="javascript">
    GrFingerX.CapStartCapture(id);
    document.getElementById('log').value ="";
    document.getElementById('log').value = document.getElementById('log').value + "Initialized\n";
    </SCRIPT>

    <SCRIPT FOR="GrFingerX" EVENT="SensorUnplug(id)" LANGUAGE="javascript">
    GrFingerX.CapStopCapture(id);
    document.getElementById('log').value = document.getElementById('log').value + "Stopped\n";
    </SCRIPT>

    <SCRIPT FOR="GrFingerX" EVENT="FingerDown(id)" LANGUAGE="javascript">
    document.getElementById('log').value = "Fingerprint Captured\n";
    </SCRIPT>

    <SCRIPT FOR="GrFingerX" EVENT="FingerUp(id)" LANGUAGE="javascript">
    document.getElementById('log').value = document.getElementById('log').value + "\n";
    </SCRIPT>


    <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Admin App</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="{{ url('admin/residents') }}">Residents</a></li>
            <li><a href="{{ url('admin/visitors') }}">Visitors</a></li>
            <li><a href="{{ url('admin/guards') }}">Guards</a></li>
            <li><a href="{{ url('admin/notice-board') }}">Notice Board</a></li>
            <li><a href="{{ url('logout') }}">Logout</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </div><!-- /.navbar -->

<div class="row">
    <div class="col-md-2">
    @include('layouts.adminsidebar')
    </div>
    <div class="col-md-10">
    @yield('content')
    </div>
</div>

    <hr>
      <footer>
        <p class="text-center">&copy; Company 2013</p>
      </footer>
<script>
//Makes sidebar nav dinamically active
  $(document).ready(function(){
    var title = document.title;
    $('.sidenav').each(function(){
      if(title.indexOf($(this).data('id')) != -1)
        $(this).addClass('active');
    });
  });
</script>
    </body>
</html>
