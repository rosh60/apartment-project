<div class="list-group">
  <a href="{{ url('dashboard/profile') }}" data-id="Profile" class="list-group-item sidenav">Profile</a>
  <a href="{{ url('messages') }}" data-id="Messages" class="list-group-item sidenav">Inbox
    <span class="badge">{{ $msgCount }}</span></a>
  <a href="{{ url('messages/create') }}" data-id="Messages" class="list-group-item sidenav">Send Message</a>
  <a href="{{ url('complaints') }}" data-id="Complaint" class="list-group-item sidenav">My Complaints
    <span class="badge">{{ $cmpCount }} </span></a>
  <a href="{{ url('complaint/register') }}" data-id="Complaint" class="list-group-item sidenav">Raise a complaint</a>
  <a href="{{ url('market') }}" data-id="Market" class="list-group-item sidenav">Buy or Sell</a>
  <a href="{{ url('forum') }}" data-id="Forum" class="list-group-item sidenav">Forum</a>
</div>