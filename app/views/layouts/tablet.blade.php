<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Visitor Registration</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
        <style type="text/css">
        .big{
          height: 200px;
          border:1px solid #ccc;
        }
        </style>
    </head>
    <body onbeforeunload="Finalize()">
<OBJECT ID="GrFingerX" CLASSID="CLSID:71944DD6-B5D2-4558-AD02-0435CB2B39DF" style="display:none;"></OBJECT>
{{ HTML::script('js/GrFinger.js') }}
<script type="text/javascript">
var i = 0;
</script>

<SCRIPT FOR="GrFingerX" EVENT="SensorPlug(id)" LANGUAGE="javascript">
GrFingerX.CapStartCapture(id);
document.getElementById('log').value ="";
document.getElementById('log').value = document.getElementById('log').value + "Initialized\n";
</SCRIPT>

<SCRIPT FOR="GrFingerX" EVENT="SensorUnplug(id)" LANGUAGE="javascript">
GrFingerX.CapStopCapture(id);
document.getElementById('log').value = document.getElementById('log').value + "Stopped\n";
</SCRIPT>

<SCRIPT FOR="GrFingerX" EVENT="FingerDown(id)" LANGUAGE="javascript">
document.getElementById('log').value = "Fingerprint Captured\n";
</SCRIPT>

<SCRIPT FOR="GrFingerX" EVENT="FingerUp(id)" LANGUAGE="javascript">
document.getElementById('log').value = document.getElementById('log').value + "\n";
</SCRIPT>

<SCRIPT FOR="GrFingerX" EVENT="ImageAcquired(id, w, h, rawImg, res)" LANGUAGE="javascript">
GrFingerX.CapSaveRawImageToFile(rawImg, w, h, "D:\\teste.bmp", 501);
Start();
sendFingerPrint(rawImg, w, h, res);
</SCRIPT>

      <div class="row">
        <div class="col-md-3 big"><img id="photo" class="big" src="http://placehold.it/200" /></div>
        <div class="col-md-3 big"><textarea id="log" cols=40 rows=3></textarea></div>
        <div class="col-md-3 big" id="personInfo">Person's info</div>
        <div class="col-md-3 big"> <IMG id="img" src="http://www.generatorstudios.com/fingerprint.gif" width="50%" /></div>
      </div>
      <div class="row">
        <div class="col-md-3">Owner / Tenant</div>
        <div class="col-md-6">Place finger again</div>
        <div class="col-md-3">{{ date('d-m-Y h:i') }}</div>
      </div>
      <div class="row">
        <div class="col-md-3 big"><img id="car_front" class="big" src="http://placehold.it/200" /></div>
        <div class="col-md-3 big"><img id="car_back" class="big" src="http://placehold.it/200" /></div>
        <div class="col-md-3 big"><img id="family0" class="big" src="http://placehold.it/200" /></div>
        <div class="col-md-3 big"><img id="family1" class="big" src="http://placehold.it/200" /></div>
      </div>
      <div class="row">
        <div class="col-md-3 big"><img id="bike_front" class="big" src="http://placehold.it/200" /></div>
        <div class="col-md-3 big"><img id="bike_back" class="big" src="http://placehold.it/200" /></div>
        <div class="col-md-3 big"><img id="family2" class="big" src="http://placehold.it/200" /></div>
        <div class="col-md-3 big"><img id="family3" class="big" src="http://placehold.it/200" /></div>
      </div>

<script type="text/javascript">

Initialize();

function Start() {
  document.getElementById("img").src = "D:\\teste.bmp?ts" + encodeURIComponent( new Date().toString() );   
}

  function sendFingerPrint(rawImg, w, h, res)
  {
    try
      {
        var result = GrFingerX.ExtractJSON(rawImg, w, h, res, GrFingerX.GR_DEFAULT_CONTEXT,GrFingerX.GR_FORMAT_ISO);
        var Objret = eval('(' + result + ')');  //Adaptate the return to an Object form
        var ret = GrFingerX.FreeJSON(result);
        if(Objret.ret < 0){
          document.getElementById('log').value = document.getElementById('log').value + "Extract Error = " + Objret.ret + "\n";
        }
        else
        {
          var http = new XMLHttpRequest();
          var url = 'tablet';
          var params = "tpt="+encodeURIComponent(Objret.tpt);
          http.open("POST", url, true);
          http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          http.setRequestHeader("Content-length", params.length);
          http.setRequestHeader("Connection", "close");
          http.onreadystatechange = function() {  //Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
              obj = JSON.parse(http.responseText);
              if(obj.ret==0)
              {
                document.getElementById('log').value = "No Matches Found.";
                resetFields();
              }
              else
              {
                $('#photo').attr('src', obj.data.photo);
                $('#car_front').attr('src', obj.data.car_front);
                $('#car_back').attr('src', obj.data.car_back);
                $('#bike_front').attr('src', obj.data.bike_front);
                $('#bike_back').attr('src', obj.data.bike_back);
                $('#personInfo').html('Door & Block : ' + obj.data.door_number + '-' + obj.data.block_number + '<br />'
                  + 'Name : ' + obj.data.name
                );
                document.getElementById('log').value = "Matched.";
                for(i=0;i<obj.data.family.length;i++)
                {
                  $('#family'+i).attr('src', obj.data.family[i]);
                }
              }
            } 
          }
          http.send(params);
        }
      }
      catch(e)
      {
        alert(e);
      }
  }

  function resetFields()
  {
    $('#photo').attr('src', '');
    $('#car_front').attr('src', '');
    $('#car_back').attr('src', '');
    $('#bike_front').attr('src', '');
    $('#bike_back').attr('src', '');
    $('#personInfo').html('');
    for(i=0;i<=4;i++)
      $('#family'+i).attr('src', '');
  }
  
</script>
    </body>
</html>