<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>@yield('title')</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
        <style type="text/css">
            #namedrop{
                position:absolute;
                width:30%;
                background:white;
                border:1px solid #ccc;
                border-radius: 5px;
                display: none;
            }
            .element{
                padding:5px 10px;
                border-radius: 5px;
                display: block;
                font-size: 16px;
            }
            .element:hover{
                background-color:#a566e7;
                color:white;
            }
        </style>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body style="width:95%; margin:0 auto; padding-top: 70px;">

    <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">App Name</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="{{ url('dashboard') }}">Home</a></li>
            <li><a href="{{ url('dashboard/profile') }}">Profile</a></li>
            <li><a href="{{ url('messages') }}">Inbox</a></li>
            <li><a href="#contact">VMS</a></li>
            <li><a href="#contact">Activities</a></li>
            <li><a href="{{ url('logout') }}">Logout</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </div><!-- /.navbar -->

    <div class="row">
        <div class="col-md-2">
        @include('layouts.residentsidebar')
        </div>
        <div class="col-md-10">
            @yield('content')
        </div>
    </div>
    <hr>
      <footer>
        <p class="text-center">&copy; Company 2013</p>
      </footer>

<script>
//Makes sidebar nav dinamically active
  $(document).ready(function(){
    var title = document.title;
    $('.sidenav').each(function(){
      if(title.indexOf($(this).data('id')) != -1)
        $(this).addClass('active');
    });
  });
</script>
    </body>
</html>
