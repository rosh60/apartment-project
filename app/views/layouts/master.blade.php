<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{ HTML::style('css/main.css') }}
    {{ HTML::style('css/visitor.css') }}
    <style>
        #namelist{
            width:150px;
            display: inline-block;
            position: absolute;
            margin:-2px 0 0 8px;
        }
        .name{
            padding:5px 8px;
            background: #AB00FF;
            border-radius:3px;
            color:white;
            cursor:pointer;
            margin:2px 5px 2px 0;
            display:inline-block;
        }
        .name:hover{
            background:#0000FF;
        }
    </style>
</head>
<body onbeforeunload="Finalize()">
<OBJECT ID="GrFingerX" CLASSID="CLSID:71944DD6-B5D2-4558-AD02-0435CB2B39DF" style="display:none;"></OBJECT>

{{ HTML::script('js/GrFinger.js') }}

<script type="text/javascript">
var i = 0;
</script>

<SCRIPT FOR="GrFingerX" EVENT="SensorPlug(id)" LANGUAGE="javascript">
GrFingerX.CapStartCapture(id);
document.getElementById('log').value ="";
document.getElementById('log').value = document.getElementById('log').value + "Initialized\n";
</SCRIPT>

<SCRIPT FOR="GrFingerX" EVENT="SensorUnplug(id)" LANGUAGE="javascript">
GrFingerX.CapStopCapture(id);
document.getElementById('log').value = document.getElementById('log').value + "Stopped\n";
</SCRIPT>

<SCRIPT FOR="GrFingerX" EVENT="FingerDown(id)" LANGUAGE="javascript">
document.getElementById('log').value = "Fingerprint Captured\n";
</SCRIPT>

<SCRIPT FOR="GrFingerX" EVENT="FingerUp(id)" LANGUAGE="javascript">
document.getElementById('log').value = document.getElementById('log').value + "\n";
</SCRIPT>



@include('layouts.guardnav')
@yield('content')
<footer></footer>
</body>
</html>