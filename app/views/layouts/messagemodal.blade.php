<div class="modal fade" id="msgModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Compose Message</h4>
      </div>
      <div class="modal-body">
        <div class="" id="msg"></div>
                    {{ Form::open(['id' => 'msgform'])}}
                    @if($errors->any())
                    {{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
                    @endif
                    @if($message = Session::get('message'))
                    {{ '<div class="alert alert-success">'.$message.'</div>' }}
                    @endif
                    <label for="subject">Message to</label>
                    {{ Form::text('name', '', ['placeholder' => 'Message to?', 'class' => 'form-control', 'id' => 'name', 'autocomplete' => 'off'])}}
                    <div id="namedrop"></div>
                    {{ Form::hidden('to', '', ['id' => 'to'])}}
                    <p></p>
                    <label for="subject">Subject</label>
                    {{ Form::text('subject', '', ['placeholder' => 'Enter Subject', 'class' => 'form-control'])}}
                    <p></p>
                    <label for="exampleInputEmail1">Message</label>
                    {{ Form::textarea('body', '', ['class' => 'form-control', 'rows' => '7']) }}    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-default" type="submit">Send Message</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$(document).ready(function(){

  $('#name').keyup(function(){
     if($('#name').val().length >= 1){
         var searchTerm = getSearchTerm();
         $.ajax({
             type: 'GET',
             url: '{{ url('api/ajaxidsearch') }}',
             data: 'search=' + searchTerm,
             success: function(response){
                  $('#namedrop').html('');
                 for (var i = 0; i < response.length; i++) {
                     $('#namedrop').html($('#namedrop').html() + '<span class="element" id="'+response[i].id+'">' + response[i].name + '</span>');
                 }
                 $('#namedrop').fadeIn();
             }
         });
     }
  });

  $('#namedrop').on('click', ".element", function(){
      $('#name').val($(this).html());
      $('#to').val($(this).attr('id'));
      $('#namedrop').fadeOut();
  });

  $('#msgform').on('submit', function() {
      $data = $(this).serialize();
      $.ajax({
          url: '{{ url('messages') }}',
          type: 'POST',
          dataType: 'json',
          data: $data,
          statusCode: {
              201: function() {
                  setAlert('Message has been sent!', 'success');
              },
              200: function(data){
                  setAlert(data.responseText, 'errors');
              }
          }
      })
      .fail(function() {
          setAlert('Error! Please try again later.', 'error');
      })          
      return false;
  });

});

function setAlert(msg, type)
{
    if(type=='success')
        $('#msg').html(msg).removeClass().addClass('alert alert-success');
    else if(type=='errors')
        $('#msg').html(msg).removeClass().addClass('alert alert-danger');
    else
        $('#msg').html(msg).removeClass().addClass('alert alert-danger');
}

function getSearchTerm()
{
    if($('#name').val().indexOf(',') != -1)
    {
        var names = $('#name').val().split(',');
        return names[names.length-1].trim();
    }
    return $('#name').val();
}

</script>