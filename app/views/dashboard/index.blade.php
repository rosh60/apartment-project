@extends('layouts.user')

@section('content')

<div class="jumbotron">
            <h1>Hello, {{ $data['name'] }}!</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque malesuada, eros eget mollis viverra, nisl tortor aliquet ipsum.</p>
</div>
<div class="row">
<div class="col-6 col-sm-6 col-lg-4">
  <h2>Notifications</h2>
  <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
  <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
</div><!--/span-->
<div class="col-6 col-sm-6 col-lg-4">
  <h2>Messages</h2>
  <p>
    @if($data['msgs']['count'] == 1)
    You have {{ $data['msgs']['count'] }} new message from {{ $data['msgs']['from'] }}.
    @elseif($data['msgs']['count'] > 1)
    You have {{ $data['msgs']['count'] }} new messages from {{ $data['msgs']['from'] }} and others.
    @else
    You have no new messages.
    @endif
  </p>
  <p><a class="btn btn-default" href="{{ url('messages') }}" role="button">View details &raquo;</a></p>
</div><!--/span-->
<div class="col-6 col-sm-6 col-lg-4">
  <h2>Visitor Updates</h2>
  <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
  <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
</div><!--/span-->
<div class="col-6 col-sm-6 col-lg-4">
  <h2>Forum Updates</h2>
  <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
  <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
</div><!--/span-->
<div class="col-6 col-sm-6 col-lg-4">
  <h2>Complaints</h2>
  <p>You have {{ $data['cmps'] }} complaint(s) in your account.</p>
  <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
</div><!--/span-->
<div class="col-6 col-sm-6 col-lg-4">
  <h2>Hot Deals</h2>
  <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
  <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
</div><!--/span-->
</div><!--/row-->

@stop