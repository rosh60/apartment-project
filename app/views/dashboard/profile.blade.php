@extends('layouts.user')

@section('title')
{{ 'User Profile' }}
@stop

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">User information</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
              @if($photo = $data->profile_photo)
              <img src="{{ url('images/residents/profile/'.$data->profile_photo) }}" class="img-thumbnail" alt="User Pic"/> 
              @else
              <img src="{{ url('images/residents/'.$data->photo) }}" class="img-thumbnail" alt="User Pic"/>
              @endif
            </div>
            <div class="col-md-5">
                <strong>{{ $data->name }}</strong><br>
                <table class="table table-condensed table-responsive table-user-information">
                    <tbody>
                    <tr>
                        <td>Block & Door No.:</td>
                        <td>{{ $data->block_number.'-'.$data->door_number }}</td>
                    </tr>
                    <tr>
                        <td>Email ID :</td>
                        <td>{{ $data->email  }}</td>
                    </tr>
                    <tr>
                        <td>Mobile :</td>
                        <td>{{ $data->mobile }}</td>
                    </tr>
                    <tr>
                        <td>DOB :</td>
                        <td>{{ $data->dob }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#msgModal"><i class="glyphicon glyphicon-envelope"></i> Send Message</button>
        <span class="pull-right">
            <a class="btn btn-sm btn-warning" type="button"
                    data-toggle="tooltip"
                    data-original-title="Edit this user" href="{{ url('dashboard/profile/edit') }}"><i class="glyphicon glyphicon-edit"></i> Edit Profile</a>
        </span>
    </div>
</div>

@include('layouts.messagemodal')

<script>
    $('#name').val('{{ $data->id }}');
    $('#id').val('{{ $data->name }}');
</script>

@stop