@extends('layouts.user')

@section('title')
Complaints
@stop

@section('content')
{{ '<div class="alert alert-info">You have '.$data->count().' complaints</div>' }}
<div class="col-md-12">
@foreach($data as $cmp)
<?php if(empty($cmp->subject)) echo 'You have no complaints' ?>
<div class="panel panel-default">
  <div class="panel-heading">{{ $cmp->subject }}</div>
    <div class="panel-body">
    <p>{{ $cmp->complaint }}</p>
  </div>
</div>
@endforeach
</div>

@stop