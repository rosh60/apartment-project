@extends('layouts.user')
@section('content')

<div class="container col-md-6">
<form action="{{ url('dashboard/profile/edit') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
	<fieldset>
		@if($errors->any())
	    {{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
	    @endif
		@if($message = Session::get('message'))
	    {{ '<div class="alert alert-success">'.$message.'</div>' }}
	    @endif
		@if($photo = Input::old('profile_photo'))
		<img src="{{ url('images/residents/profile/'.$photo) }}" class="img-thumbnail" width="250px"/> 
		@else
		<img src="{{ url('images/residents/'.Input::old('photo')) }}" class="img-thumbnail" width="250px"/> 
		@endif
		{{ Form::file('profile_photo') }}<br />

	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Name</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">{{ Input::old('name') }}</p>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Block & Door #</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">{{ Input::old('block_number').'-'.Input::old('door_number') }}</p>
		    </div>
	  	</div>
		<div class="form-group">
			<label for="occupation" class="col-sm-2 control-label">Occupation</label>
		    <div class="col-sm-10">
		    	{{ Form::text('occupation', '', ['required' => '', 'class'=>"form-control" ]) }}
		    </div>
	  	</div>
		<div class="form-group">
			<label for="occupation" class="col-sm-2 control-label">Mobile</label>
		    <div class="col-sm-10">
		    	{{ Form::text('mobile', '', ['required' => '', 'class'=>"form-control" ]) }}
		    </div>
	  	</div>
	  	<div class="form-group">
			<label for="occupation" class="col-sm-2 control-label">Email ID</label>
		    <div class="col-sm-10">
		    	{{ Form::text('email', '', ['required' => '', 'class'=>"form-control" ]) }}
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">DOB</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">{{ Input::old('dob') }}</p>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Blood Group</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">{{ Input::old('blood_group') }}</p>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Identity Proof</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">Undefined</p>
		    </div>
	  	</div>
	  	<div class="form-group">
			<label for="occupation" class="col-sm-2 control-label">Status</label>
		    <div class="col-sm-10">
		    	{{ Form::text('status', '', ['required' => '', 'class'=>"form-control" ]) }}
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Unread Messages</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">{{ Input::old('messages') }}</p>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Unread Notification</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">Undefined</p>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Car Image Front</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">{{ Form::file('car_front', array()); }}</p>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Car Image Back</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">{{ Form::file('car_back', array()); }}</p>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Car Image Back</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">{{ Form::file('bike_front', array()); }}</p>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label class="col-sm-2 control-label">Car Image Back</label>
		    <div class="col-sm-10">
		      <p class="form-control-static">{{ Form::file('bike_back', array()); }}</p>
		    </div>
	  	</div>
	</fieldset>
	<input type="submit" value="Update" class="btn btn-default" />
</form>
</div>

@stop