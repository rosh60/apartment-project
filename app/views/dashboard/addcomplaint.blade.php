@extends('layouts.user')

@section('title')
Complaints
@stop

@section('content')

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">    
                {{ Form::open(['url' => 'complaint/register', 'method' => 'post'])}}
                <legend>Raise a complaint</legend>
                @if($errors->any())
                {{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
                @endif
                @if($message = Session::get('message'))
                {{ '<div class="alert alert-success">'.$message.'</div>' }}
                @endif
                <label for="subject">Subject</label>
                {{ Form::text('subject', '', ['placeholder' => 'Enter Subject', 'class' => 'form-control'])}}
                <p></p>
                <label for="exampleInputEmail1">Complaint</label>
                {{ Form::textarea('complaint', '', ['class' => 'form-control', 'rows' => '12']) }}
                <p></p>
                <div class="row">
                    <div class="col-md-10">
                    <label class="radio-inline">
                        <input type="radio" name="type" id="inlineCheckbox1" value="0" checked />
                        Personal Complaint
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="type" id="inlineCheckbox2" value="1" />
                        General Complaint
                    </label>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-default" type="submit">Submit Complaint</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop