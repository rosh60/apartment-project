@extends('layouts.master')

@section('content')

<div class="span12 centerdiv">
    {{ Form::open(['url' => 'guard/add', 'method' => 'post', 'class' => 'form-horizontal']) }}
        <legend>Add New Guard</legend>
        @if($errors->any())
        {{ '<div class="alert alert-error">'.implode('<br>', $errors->all()).'</div>' }}
        @endif
        @if($message = Session::get('message'))
        {{ '<div class="alert alert-success">'.$message.'</div>' }}
        @endif
        <div style="width:330px;float:left;">
            <div id="webcam" style="">
            </div>
            <a class="btn btn-small" id="btn1" onclick="base64_tofield();base64_toimage()">Take Snapshot</a>
        </div>
        <div style="width:200px;float:left;">
            <img id="image" style="width:200px;"/>
        </div>
        {{ Form::textarea('photo', '', ['id' => 'formfield', 'style' => 'visibility:hidden;'])}}

        <fieldset>
            <div class="control-group">
                <label class="control-label">Guard Name</label>
                <div class="controls">
                    {{ Form::text('name', '', ['required' => '', 'placeholder' => 'Guard Name', 'class' => 'input-xlarge']) }}
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Date of Birth</label>
                <div class="controls">
                    {{ Form::text('dob', '', ['required' => '', 'placeholder' => 'DD-MM-YYYY', 'class' => 'input-xlarge']) }}
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Gender</label>
                <div class="controls">
                    <label class="radio">
                        {{ Form::radio('gender', '1') }}
                        Male
                    </label>
                    <label class="radio">
                        {{ Form::radio('gender', '0') }}
                        Female
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Local Address</label>
                <div class="controls">
                    {{ Form::text('laddress', '', ['required' => '', 'placeholder' => 'Local address', 'class' => 'input-xlarge']) }}
                    <p class="help-block">Apartment, Street address, City etc.</p>
                </div>
            </div>
            <!-- address-line2 input-->
            <div class="control-group">
                <label class="control-label">Permanent Address</label>
                <div class="controls">
                    {{ Form::text('paddress', '', ['required' => '', 'placeholder' => 'Permanent address', 'class' => 'input-xlarge']) }}
                    <p class="help-block">Apartment, Street address, City etc.</p>
                </div>
            </div>
            <!-- city input-->
            <div class="control-group">
                <label class="control-label">Mobile</label>
                <div class="controls">
                    {{ Form::text('mobile', '', ['required' => '', 'placeholder' => 'Mobile Number', 'class' => 'input-xlarge']) }}
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Place of origin</label>
                <div class="controls">
                    {{ Form::text('origin', '', ['required' => '', 'placeholder' => 'Place of origin', 'class' => 'input-xlarge']) }}
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Login & Logout Time</label>
                <div class="controls">
                    {{ Form::text('login', '', ['required' => '', 'placeholder' => 'HH:MM', 'class' => 'input-small']) }}
                    &nbsp; To : &nbsp;
                    {{ Form::text('logout', '', ['required' => '', 'placeholder' => 'HH:MM', 'class' => 'input-small']) }}
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Position</label>
                <div class="controls">
                    {{ Form::text('position', '', ['required' => '', 'placeholder' => 'Position', 'class' => 'input-xlarge']) }}
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-large btn-primary">Submit Now</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
{{ HTML::script('js/scriptcam.js') }}
<script language="JavaScript">
    $(document).ready(function() {
        $("#webcam").scriptcam({
            showMicrophoneErrors:false,
            onError:onError,
            cornerRadius:20,
            cornerColor:'e3e5e2',
            onWebcamReady:onWebcamReady,
            onPictureAsBase64:base64_tofield_and_image,
            path:'{{ URL::to("") }}/js/'
        });
    });
    function base64_tofield() {
        $('#formfield').val($.scriptcam.getFrameAsBase64());
    };
    function base64_toimage() {
        $('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64()).slideDown();
    };
    function base64_tofield_and_image(b64) {
        $('#formfield').val(b64);
        $('#image').attr("src","data:image/png;base64,"+b64);
    };
    function changeCamera() {
        $.scriptcam.changeCamera($('#cameraNames').val());
    }
    function onError(errorId,errorMsg) {
        $( "#btn1" ).attr( "disabled", true );
        $( "#btn2" ).attr( "disabled", true );
        alert(errorMsg);
    }
    function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
        $.each(cameraNames, function(index, text) {
            $('#cameraNames').append( $('<option></option>').val(index).html(text) )
        });
        $('#cameraNames').val(camera);
    }
</script>

@stop