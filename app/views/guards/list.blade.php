@extends('layouts.master')
@section('content')

<div class="list">
<table class="table table-bordered">
    <tr>
        <td>Name</td>
        <td>Mobile</td>
        <td>Login</td>
        <td>Logout</td>
        <td>Position</td>
        <td>Photo</td>
    </tr>
    <?php foreach($data as $row): ?>
        <tr>
            <td><?=$row->name?></td>
            <td><?=$row->mobile?></td>
            <td><?=$row->login?></td>
            <td><?=$row->logout?></td>
            <td><?=$row->position?></td>
            <td><img src="data:image/jpg;base64,<?=$row->photo?>" /></td>
        </tr>
    <?php endforeach; ?>
</table>
</div>

@stop