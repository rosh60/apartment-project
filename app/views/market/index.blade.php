@extends('layouts.user')

@section('title')
Market
@stop

@section('content')
<div class="alert alert-info">
	<div class="col-md-3"></div>
	Have somethig to sell or rent?
	<a href="{{ URL::to('market/additem') }}" class="btn btn-primary">Post an item now!</a>
</div>
<div class="col-md-12">
@foreach($data as $item)
<div class="panel panel-default">
  <div class="panel-heading"><strong>{{ $item->name }}</strong><p class="pull-right"><strong>Rs : {{ $item->price }}</strong></p></div>
    <div class="panel-body">
    <p>{{ $item->description }}</p>
    <a href="{{ URL::to('market/item/'.$item->id) }}" class="btn btn-primary btn-sm">Read More!</a>
  </div>
</div>
@endforeach
</div>

@stop