@extends('layouts.user')

@section('content')

<div class="well col-md-10" style="text-align:center;">
          <img src="{{ url('/images/useruploads/'.$data->picture) }}" width="400px">
          <h2>{{ $data->name }}</h2>
          <p>{{ $data->description }}</p>
          <p><table border="1" align="center">
          	<tr>
          		<td>For Sale</td>
          		<td>{{ $data->for_sale }}</td>
          	</tr>
          	<tr>
          		<td>For Rent</td>
          		<td>{{ $data->for_rent }}</td>
          	</tr>
          	<tr>
	          	<td>Price</td>
	          	<td>Rs : {{ $data->price }}</td>
          	</tr>
          </table><p></p>
          <p><a class="btn btn-default" href="{{ url('dashboard/profile/'.$data->uid) }}">Show Seller's Profile</a></p>
          <p>Seller : {{ $data->resident->name }} ({{ $data->resident->mobile }}).</p>
</div>
<div class="well col-md-10">
     <div id="comments"></div>
	{{ Form::open(['url' => Request::url(), 'method' => 'post', 'id' => 'commentform'])}}
	  <div class="form-group">
	    {{ Form::text('reply', '', ['placeholder' => 'Post a reply', 'class' => 'form-control'])}}
	  </div>
	  <button type="submit" class="btn btn-primary pull-right">Post Reply</button>
	</form>
</div>


<script>

$(document).ready(function(){
     getComments();
     $('#commentform').on('submit', function(){
          data = $(this).serialize();
          $.ajax({
            type: 'POST',
            url: '',
            data: data,
            success: function(response){
                    getComments();
               }
          });
          return false;
     });
});

function getComments()
{
     $("#comments").hide();
     $.ajax({
          type: 'GET',
          url: document.URL+'/comments',
          success: function(response){
               var profileurl = '{{ URL::to('dashboard/profile') }}/';
               var cmnts = '';
               for(i=0;i<response.length;i++){
                    cmnts += '<a href="'+profileurl+response[i].id+'">'+response[i].name+'</a> (<i>'+ response[i].created_at +'</i>) :<p>'+response[i].comment+'</p><hr />';
               }
               $("#comments").html(cmnts);
          }
     });
     $("#comments").fadeIn('slow');
}

</script>

@stop