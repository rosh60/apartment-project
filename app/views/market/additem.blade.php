@extends('layouts.user')
@section('content')

<div class="well col-md-10">

{{ Form::open(['url' => 'market/additem', 'method' => 'post', 'enctype'=>'multipart/form-data']) }}
    <legend>Post an item for Sale / Rent</legend>
    @if($errors->any())
    {{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
    @endif
    @if($message = Session::get('message'))
    {{ '<div class="alert alert-success">'.$message.'</div>' }}
    @endif
  <div class="form-group">
    <label for="name">Name</label>
    {{ Form::text('name', '', ['placeholder' => 'Enter name of the item', 'class' => 'form-control']) }}
  </div>
  <div class="form-group">
    <label for="description">Description</label>
    {{ Form::textarea('description', '', ['class' => 'form-control']) }}
  </div>
  <div class="form-group">
    <label for="name">Price</label>
    {{ Form::text('price', '', ['placeholder' => 'Enter amount', 'class' => 'form-control']) }}
  </div>
  <div class="form-group">
    <label for="picture">Upload Picture</label>
    <input type="file" name="picture" class="form-control">
  </div>
  Available for?
  	<label class="checkbox-inline">
    	<input type="hidden" name="for_rent" value="N" />
      	<input name="for_rent" type="checkbox" value="Y"> Rent
    </label>
    <label class="checkbox-inline">
    	<input type="hidden" name="for_sale" value="N" />
      	<input name="for_sale" type="checkbox" value="Y"> Sale
    </label>
  <button type="submit" class="btn btn-primary btn-lg pull-right">Submit</button>
</form>
</div>

@stop