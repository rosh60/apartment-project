@extends('layouts.master')

@section('content')

<SCRIPT FOR="GrFingerX" EVENT="ImageAcquired(id, w, h, rawImg, res)" LANGUAGE="javascript">
GrFingerX.CapSaveRawImageToFile(rawImg, w, h, "D:\\teste.bmp", 501);
Start();
if(document.getElementById('img').style.display == 'none')
    document.getElementById('img').style.display = 'block';
CallEnroll(rawImg, w, h, res);
</SCRIPT>

<div class="span12 centerdiv">
    {{ Form::open(['url' => 'resident/add', 'method' => 'post', 'class' => 'form-horizontal'])}}
    <legend>Add New Resident</legend>
    @if($errors->any())
    {{ '<div class="alert alert-error">'.implode('<br>', $errors->all()).'</div>' }}
    @endif
    @if($message = Session::get('message'))
    {{ '<div class="alert alert-success">'.$message.'</div>' }}
    @endif
    <div style="width:330px;float:left;">

        <input type = "button" value = "Take Fingerprint" onclick = "Initialize()" />

        <IMG id="img" src="http://www.generatorstudios.com/fingerprint.gif" width="50%" border="1" name="refresh" />
        <SCRIPT language="JavaScript" type="text/javascript">
        <!--
        function Start() {
        document.getElementById("img").src = "D:\\teste.bmp?ts" + encodeURIComponent( new Date().toString() );
        }
        </SCRIPT> 
        {{ Form::hidden('fingerprint', '', ['id' => 'fingerprint'])}}
        <textarea name="log" id = "log" rows = "5" cols = "75" ></textarea>

        <div id="webcam" style="">
        </div>
        <a class="btn btn-small" id="btn1" onclick="base64_tofield();base64_toimage()">Take Snapshot</a>
        <div style="width:200px;"><br>
            <img id="image" style="width:200px;"/>
        
        </div>
        {{ Form::textarea('photo', '', ['id' => 'formfield', 'style' => 'display:block; visibility:hidden;']) }}

        </div>

<fieldset>
    <div class="control-group">
        <label class="control-label">Block & Door No.</label>
        <div class="controls">
            {{ Form::select('block_number', [
            '' => 'Block',
            'A' => 'A',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D',
            'E' => 'E',
            'F' => 'F'
            ], 'Block', ['class' => 'input-small', 'required' => '']) }}
            {{ Form::text('door_number', '', ['required' => '', 'maxlength' => '3', 'placeholder' => 'Door Number', 'class' => 'input-small']) }}
            <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Full Name</label>
        <div class="controls">
            <p class="help-block"></p>
            {{ Form::text('fullname', '', ['required' => '', 'placeholder' => 'Full Name', 'class' => 'input-xlarge'])}}
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Date of Birth</label>
        <div class="controls">
            {{ Form::text('dob', '', ['required' => '', 'placeholder' => 'DD-MM-YYYY', 'class' => 'input-xlarge'])}}
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Gender</label>
        <div class="controls">
            <label class="radio inline">
                {{ Form::radio('gender', '1') }}
                Male
            </label>
            <label class="radio inline">
                {{ Form::radio('gender', '0') }}
                Female
            </label>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Blood Group</label>
        <div class="controls">
            {{ Form::select('blood_group', [
            '' => 'Blood Group',
            'A Positive' => 'A Positive',
            'A Negetive' => 'A Negetive',
            'B Positive' => 'B Positive',
            'B Negetive' => 'B Negetive',
            'O Positive' => 'O Positive',
            'O Negetive' => 'O Negetive',
            'AB Positive' => 'AB Positive',
            'AB Negetive' => 'AB Negetive'
            ], 'Block', ['class' => 'input-xlarge', 'required' => '']) }}
            <p class="help-block"></p>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">Mobile</label>
        <div class="controls">
            {{ Form::text('mobile', '', ['required' => '', 'placeholder' => 'Mobile Number', 'class' => 'input-xlarge'])}}
            <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Occupation</label>
        <div class="controls">
            {{ Form::text('occupation', '', ['required' => '', 'placeholder' => 'Engineer, Student etc.', 'class' => 'input-xlarge'])}}
            <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">E-mail ID</label>
        <div class="controls">
            {{ Form::email('email', '', ['required' => '', 'placeholder' => 'E-Mail Address', 'class' => 'input-xlarge'])}}
            <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Vehicle No.s</label>
        <div class="controls">
            {{ Form::text('vehicle_nos', '', ['placeholder' => 'Comma seperate multiple numbers', 'class' => 'input-xlarge'])}}
            <p class="help-block"></p>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button class="btn btn-large btn-primary">Submit Now</button>
        </div>
    </div>
</fieldset>
</form>
</div>

<script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
{{ HTML::script('js/scriptcam.js') }}
<script language="JavaScript">
    $(document).ready(function() {
        $("#webcam").scriptcam({
            showMicrophoneErrors:false,
            onError:onError,
            cornerRadius:20,
            cornerColor:'e3e5e2',
            onWebcamReady:onWebcamReady,
            onPictureAsBase64:base64_tofield_and_image,
            path:'{{ URL::to("") }}/js/'
        });
    });
    function base64_tofield() {
        $('#formfield').val($.scriptcam.getFrameAsBase64());
    };
    function base64_toimage() {
        $('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64()).slideDown();
    };
    function base64_tofield_and_image(b64) {
        $('#formfield').val(b64);
        $('#image').attr("src","data:image/png;base64,"+b64);
    };
    function changeCamera() {
        $.scriptcam.changeCamera($('#cameraNames').val());
    }
    function onError(errorId,errorMsg) {
        $( "#btn1" ).attr( "disabled", true );
        $( "#btn2" ).attr( "disabled", true );
        alert(errorMsg);
    }
    function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
        $.each(cameraNames, function(index, text) {
            $('#cameraNames').append( $('<option></option>').val(index).html(text) )
        });
        $('#cameraNames').val(camera);
    }
</script>

@stop