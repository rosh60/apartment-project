@extends('layouts.master')

@section('content')

<div class="list">
<table class="table table-bordered">
    <tr>
        <td>Block & Door No.</td>
        <td>Name</td>
        <td>Mobile</td>
        <td>E-mail</td>
        <td>OTP</td>
        <td>Photo</td>
    </tr>
    <?php foreach($data as $row): ?>
        <tr>
            <td><?=$row->block_number.$row->door_number?></td>
            <td><?=$row->name?></td>
            <td><?=$row->mobile?></td>
            <td><?=$row->email?></td>
            <td><?=$row->otp?></td>
            <td><img src="{{ URL::to('images/residents').'/'.$row->id.'.jpg' }}" /></td>
        </tr>
    <?php endforeach; ?>
</table>
</div>

@stop