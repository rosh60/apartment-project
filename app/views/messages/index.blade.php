@extends('layouts.user')

@section('title')
Messages
@stop

@section('content')
<div class="col-md-12">
@if($message = Session::get('message'))
{{ '<div class="alert alert-success">'.$message.'</div>' }}
@endif
<button class="btn btn-primary" data-toggle="modal" data-target="#msgModal">
  Send Message
</button>
<p></p>
<table class="table table-bordered table-striped">
	<tr>
		<td><strong><strong>Sender</strong></td>
		<td><strong>Subject</strong></td>
		<td></td>
	</tr>
	@foreach($data as $msg)
	<tr>
		<td>{{ $msg->sender->name }}</td>
		<td><a href="{{ url('messages/'.$msg->id) }}"><strong>{{ $msg->subject }}</strong></a></td>
		<td>
			{{ Form::open(array('method' => 'DELETE', 'route' => array('messages.destroy', $msg->id))) }}
			{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
			{{ Form::close() }}
		</td>
	</tr>
	@endforeach
</table>
</div>

@include('layouts.messagemodal')


@stop
