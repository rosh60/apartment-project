@extends('layouts.user')

@section('content')

<div class="well col-md-10" style="text-align:center;">
          <table class="table table-bordered">
                    <tr><td>From : {{ $msg->sender->name }}</td></tr>
                    <tr><td>Subject : {{ $msg->subject }}</td></tr>
                    <tr><td>Message : {{ $msg->body }}</td></tr>
          </table>
</div>

@stop