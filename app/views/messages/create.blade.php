@extends('layouts.user')

@section('title')
Messages
@stop

@section('content')

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">            
                {{ Form::open(['url' => 'messages', 'method' => 'post'])}}
                    <legend>Send Message</legend>
                    @if($errors->any())
                    {{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
                    @endif
                    @if($message = Session::get('message'))
                    {{ '<div class="alert alert-success">'.$message.'</div>' }}
                    @endif
                    <label for="subject">Message to</label>
                    {{ Form::text('name', '', ['placeholder' => 'Message to?', 'class' => 'form-control', 'id' => 'name', 'autocomplete' => 'off'])}}
                    <div id="namedrop"></div>
                    {{ Form::hidden('to', '', ['id' => 'to'])}}
                    <p></p>
                    <label for="subject">Subject</label>
                    {{ Form::text('subject', '', ['placeholder' => 'Enter Subject', 'class' => 'form-control'])}}
                    <p></p>
                    <label for="exampleInputEmail1">Message</label>
                    {{ Form::textarea('body', '', ['class' => 'form-control', 'rows' => '7']) }}
                    <p></p>
                    <button class="btn btn-default" type="submit">Send Message</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('#name').keyup(function(){
           if($('#name').val().length >= 1){
               $.ajax({
                   type: 'GET',
                   url: 'ajaxidsearch',
                   data: 'search=' + $('#name').val(),
                   success: function(response){
                        $('#namedrop').html('');
                       for (var i = 0; i < response.length; i++) {
                           $('#namedrop').html($('#namedrop').html() + '<span class="element" id="'+response[i].id+'">' + response[i].name + '</span>');
                       }
                       $('#namedrop').fadeIn();
                   }
               });
           }
        });

        $('#namedrop').on('click', ".element", function(){
            $('#name').val($(this).html());
            $('#to').val($(this).attr('id'));
            $('#namedrop').fadeOut();
        });

    });
</script>

@stop