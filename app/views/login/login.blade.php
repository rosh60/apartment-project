@extends('layouts.master')

@section('content')

<div id="login">
    <form action="{{ URL::to('login') }}" method="POST" class="form-signin" style="width: 300px; margin-bottom:20px; ">
        <h2 class="form-signin-heading">Guard Login</h2>
        @if($errors->any())
        {{ implode('<br>', $errors->all()) }}
        @endif
        @if($message = Session::get('message'))
        {{ $message }}
        @endif
        <input type="email" class="input-block-level" placeholder="Email address" value="test@test.com" name="email">
        <input type="password" class="input-block-level" placeholder="Password" value="test" name="password">
        <label class="checkbox">
            <input type="hidden" name="remember" value="N" />
            <input name="remember" type="checkbox" value="Y"> Remember me
        </label>
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
    </form>
    <form action="{{ URL::to('login') }}" method="POST" class="form-signin" style="width: 300px; ">
        <h2 class="form-signin-heading">Resident Login</h2>
        <input type="email" class="input-block-level" placeholder="Email address" name="email">
        <input type="password" class="input-block-level" placeholder="Password" name="password">
        <label class="checkbox">
            <input type="hidden" name="remember" value="0" />
            <input name="remember" type="checkbox" value="1"> Remember me
        </label>
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
    </form>
</div>

@stop