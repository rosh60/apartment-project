@extends('layouts.user')

@section('title')
{{ 'Show Thread' }}
@stop

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $thread->subject }}</h3>
    </div>
    @foreach($thread->posts as $post)
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2" style="border-right:1px solid #ccc;">
                <div class="panel-body text-center">
                    @if($photo = $post->resident->profile_photo)
                        <img src="{{ url('images/residents/profile/'.$photo) }}" class="img-thumbnail" alt="User Pic"/> 
                    @else
                        <img src="{{ url('images/residents/'.$post->resident->photo) }}" class="img-thumbnail" alt="User Pic"/>
                    @endif
                    <div class="mic-info">
                        <a href="#">{{ $post->resident->name }}</a><br />
                        {{ $post->resident->block_number.'-'.$post->resident->door_number }}<br />
                        {{ $dt->parse($post->created_at)->diffForHumans() }}
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                {{ $post->content }}
            </div>
        </div>
    </div>
    <div class="panel-footer">
    	@if($post->resident->id == Auth::user()->id)
        <a class="btn btn-primary btn-xs" href="{{ url('forum/editpost/'.$post->id) }}">
            <span class="glyphicon glyphicon-pencil"></span> Edit
        </a>
        <a class="btn btn-danger btn-xs" href="{{ url('forum/deletepost/'.$post->id) }}">
            <span class="glyphicon glyphicon-trash"></span> Delete
        </a>
        @endif
    </div>
    @endforeach
</div>

@if($errors->any())
{{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
@endif

<div class="row">
	<div class="col-sm-4 col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">                
                <form accept-charset="UTF-8" action="{{ url('forum/newreply/'.$thread->id) }}" method="POST">
                    <textarea class="form-control counted" name="post" placeholder="Type in your message" rows="5" style="margin-bottom:10px;"></textarea>
                    <button class="btn btn-default" type="submit">Post New Reply</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop