@extends('layouts.user')

@section('title')
{{ 'Edit Post' }}
@stop

@section('content')

@if($errors->any())
{{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
@endif

<div class="row">
	<div class="col-sm-4 col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">                
                <form accept-charset="UTF-8" action="{{ url('forum/editpost/'.$post->id) }}" method="POST">
                    <textarea class="form-control counted" name="post" placeholder="Type in your message" rows="5" style="margin-bottom:10px;">{{ $post->content }}</textarea>
                    <button class="btn btn-info" type="submit">Update Reply</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop