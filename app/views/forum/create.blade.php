@section('title')
{{ 'Create new topic' }}
@stop

@section('content')

<h1>Create New Topic</h1>

@if($errors->any())
{{ '<div class="alert alert-danger">'.implode('<br>', $errors->all()).'</div>' }}
@endif

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-body">                
            <form accept-charset="UTF-8" action="{{ url('forum/newthread') }}" method="POST">
                {{ Form::text('subject', '', array('class' => 'form-control',  'style'=>'margin-bottom:10px;', 'placeholder' => 'Enter topic name here')) }}
                <textarea class="form-control" name="content" placeholder="Type in your message" rows="15" style="margin-bottom:10px;"></textarea>
                <button class="btn btn-default" type="submit">Create New Topic</button>
            </form>
        </div>
    </div>
</div>

@stop


