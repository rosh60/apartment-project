@extends('layouts.user')

@section('title')
{{ 'Discussion Forum' }}
@stop

@section('content')

<div class="row">
<div class="col-md-10"><h1>Discussion Forum</h1></div>
<div class="col-md-2"><a href="{{ url('forum/newthread') }}" class="btn btn-primary" style="margin-top:25px;">Create New Topic</a></div>
</div>

@if (count($threads))

<div class="container">
    <div class="row">
        <div class="panel panel-default widget">
            <div class="panel-heading">
                <h3 class="panel-title">Recent Discussions</h3>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                	@foreach ($threads as $thread)
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-10 col-md-11">
                                <a href="{{ url('forum/showthread/'.$thread['id']) }}">
                                    {{ $thread->subject }}</a>
                                <div class="mic-info">
                                    By: <a href="{{ url('dashboard/profile/'.$thread->resident->id) }}">{{ $thread->resident->name }}</a> on {{ $thread->created_at }}
                                </div>
                            </div>
                            <div class="col-xs-10 col-md-1">
                            	@if($thread->resident->id == Auth::user()->id)
                                <a class="btn btn-danger" href="{{ url('forum/deletethread/'.$thread->id) }}">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                                @endif
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>

@else
	There are no topics.
@endif

@stop
