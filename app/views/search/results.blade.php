@extends('layouts.master')

@section('content')

<div class="list">
    <table class="table table-bordered table-striped">
        <?php foreach($data as $ele): ?>
        <tr>
            <td><a href="{{ URL::to('resident/profile').'/'.$ele->id }}"><?=$ele->name?></a><br>
                <?=$ele->block_number.$ele->door_number?>, <?=$ele->address?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>

@stop