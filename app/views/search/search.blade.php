@extends('layouts.master')

@section('content')

<div class="span12 centerdiv">
	{{ Form::open(['url' => 'search/results', 'method' => 'post', 'class' => 'form-horizontal'])}}
        <legend>Search</legend>
        <fieldset>
            <div class="control-group">
                <label class="control-label">Search : </label>
                <div class="controls">
                    <input type="text" id="search" data-provide="typeahead" name="search" autocomplete="off">
                    <button class="btn btn-medium btn-primary">Search</button>
                </div>
            </div>
        </fieldset>
    </form>
<br>
	{{ Form::open(['url' => 'search/visitor-search', 'method' => 'post', 'class' => 'form-horizontal'])}}
        <legend>Visitor Search</legend>
        <fieldset>
            <div class="control-group">
                <div class="controls">
                    <input type="text" id="visitor_search" name="visitor_search" placeholder="Search query" autocomplete="on">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input type="text" id="date_from" class="input-small" name="date_from" placeholder="Date From" autocomplete="on">
                    <input type="text" id="date_to" class="input-small" name="date_to" placeholder="Date To" autocomplete="on">
                    <button class="btn btn-medium btn-primary">Search</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<script language="JavaScript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
{{ HTML::script('js/bootstrap.min.js') }}
<script language="JavaScript">
    $(document).ready(function() {
        $('#search').typeahead({
            source: function(query, process){
                $.ajax({
                    type: 'POST',
                    url: 'search',
                    data: 'search=' + query,
                    dataType: 'JSON',
                    async: true,
                    success: function(data){
                        process(data);
                    }
                });
            }
        });
    });
</script>

@stop