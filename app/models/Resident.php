<?php

class Resident extends Eloquent {

	protected $softDelete = true;
	public $fillable = ['cpu_speed', 'device_type', 'gcm_token', 'mid', 'make_model', 'os_version', 'otp', 'screen_height', 'screen_width', 'latitude', 'longitude', 'car_front', 'car_back', 'bike_front', 'bike_back', 'user_status', 'membership'];
	public $visible = array();
	public static $rules = array(
				'block_number' => 'required|in:A,B,C,D,E,F',
				'door_number' => 'required|numeric',
				'name' => 'required',
				'dob' => 'date|required',
				'gender' => 'required',
				'blood_group' => 'required',
				'mobile' => 'required|numeric|digits:10',
				'intercomm' => 'required|numeric',
				'occupation' => 'required',
				'email' => 'required|email',
				'photo' => 'required',
				'user_status' => 'required|in:Owner,Tenant'
			);

	public function messages()
	{
		return $this->hasMany('Message', 'from');
	}

	public function complaint()
	{
		return $this->hasMany('Complaint', 'uid');
	}

	public function topics()
	{
		return $this->hasMany('Topic', 'uid');
	}

	public function posts()
	{
		return $this->hasMany('Post', 'uid');
	}

	public function staff()
	{
		return $this->belongsToMany('Admin\Models\Staff', 'resident_staff');
	}

	public function visitors()
	{
		return $this->hasMany('Visitor', 'uid');
	}

	public function vehicles()
	{
		return $this->hasMany('Vehicle', 'uid');
	}

	static function generateOtp($name, $bno, $dno, $dob)
	{
		$name = substr(str_replace(' ', '', $name), 0, 4);
		return strtoupper($name.$bno.$dno.date('dy', strtotime($dob)).date('y'));
	}

	static function generateUid($otp)
	{
		return substr(md5($otp), 0, 10);
	}

	public static function getIdByName($name)
	{
		$result = DB::select('SELECT id, name FROM residents WHERE name LIKE ?', ['%'.$name.'%']);
		return Response::json($result);
	}

}