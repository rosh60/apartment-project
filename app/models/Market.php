<?php

class Market extends Eloquent{

	protected $table = 'market';
	protected $fillable = ['name', 'description', 'picture', 'for_sale', 'for_rent', 'price', 'uid'];

	public static $rules = [
		'name' => 'required',
		'description' => 'required',
		'price' => 'required|numeric',
		'picture' => 'required|mimes:jpeg,png'
	];

	public function resident()
	{
		return $this->belongsTo('Resident', 'uid');
	}
	
}