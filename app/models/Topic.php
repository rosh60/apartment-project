<?php

class Topic extends Eloquent
{

	protected $fillable = ['subject', 'uid'];
	public static $rules = [
				'subject' => 'required',
				'content' => 'required'
	];

	public function posts()
	{
		return $this->hasMany('Post');
	}

	public function resident()
	{
		return $this->belongsTo('Resident', 'uid');
	}

}