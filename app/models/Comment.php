<?php

use Carbon\Carbon;

class Comment extends Eloquent{

	protected $fillable = ['comment', 'item_id', 'uid'];

	public static function getCommentsWithName()
	{
		$cmnts = DB::select('SELECT 
    						comments.comment, comments.created_at, residents.name, residents.id
							FROM comments
							JOIN residents ON comments.uid = residents.id WHERE comments.item_id = '.Request::segment(3));
		foreach($cmnts as $cmnt)
		{
			$c = new Carbon($cmnt->created_at);
			$cmnt->created_at = $c->diffForHumans();
		}
		return $cmnts;
	}

}