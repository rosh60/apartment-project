<?php

class Vehicle extends Eloquent
{
	protected $table = 'vehicles';
	protected $fillable = ['slot', 'sticker', 'model', 'number', 'type', 'front', 'back', 'uid'];
	public static $rules = [
							'slot' 		=> 'required',
							'sticker' 	=> 'required|numeric',
							'model' 	=> 'required',
							'number' 	=> 'required',
							'type' 		=> 'required|in:2 Wheeler,4 Wheeler',
							'front'		=> 'mimes:jpeg',
							'back'		=> 'mimes:jpeg',
						];

	public function Resident()
	{
		return $this->belogsTo('Resident', 'uid');
	}
}