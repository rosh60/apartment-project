<?php

class Message extends Eloquent
{

	protected $fillable = ['from', 'to', 'subject', 'body'];

	public static $rules = [
						'to' => 'required|alpha_num',
						'subject' => 'required',
						'body' => 'required'
					];

	public function sender()
	{
		return $this->belongsTo('Resident', 'from');
	}

}