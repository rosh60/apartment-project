<?php

class Complaint extends Eloquent{

	protected $fillable = ['subject', 'complaint', 'uid'];
	private $_input;
	public $v;

	public function resident()
	{
		return $this->belongsTo('Resident', 'uid');
	}

	function __construct($data = false)
	{
		if ($data != false) $this->_input = $data;
	}

	public function validateInput()
	{
		$rules = ['subject' => 'required', 'complaint' => 'required', 'type' => 'required|in:0,1'];
		$this->v = Validator::make($this->_input, $rules);
		return $this->v->passes();
	}

	public function add()
	{
		$temp = Common::sanitize($this->_input);
		$temp['uid'] = Auth::user()->id;
		parent::__construct($temp);
		return parent::save();
	}

	public function replies()
	{
		return $this->hasMany('Admin\Models\Reply', 'complaint_id');
	}

}