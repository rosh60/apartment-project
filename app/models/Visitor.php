<?php

class Visitor extends Eloquent{

	protected $fillable = ['block_number', 'door_number', 'visitor_name', 'person_to_visit', 'purpose', 'address', 'mobile', 'gender', 'photo', 'date', 'guard_id'];
	private $_input;
	public $timestamps = false;
	public $v;

	public function __construct($data = null)
	{
		if (!$data == null) $this->_input = $data;
	}

	public function resident()
	{
		return $this->belongsTo('Resident', 'uid');
	}

	public function validateInput()
	{
		$rules = [
			'block_number' => 'required|alpha',
			'door_number' => 'required|numeric',
			'visitor_name' => 'required',
			'person_to_visit' => 'required',
			'purpose' => 'required',
			'address' => 'required',
			'mobile' => 'required|numeric|min:7000000000|max:9999999999',
			'gender' => 'required',
			'photo' => 'required'
		];
		$this->v = Validator::make($this->_input, $rules);
		return $this->v->passes();
	}

	public function add()
	{
		$temp = $this->_input;
		$temp = Common::sanitize($temp);

		$filename = md5(time().$temp['visitor_name']).'.jpg';
		file_put_contents(public_path('images/visitors/'.$filename), base64_decode($temp['photo']));
		$temp['photo'] = $filename;
		$temp['date'] = date('Y-m-d H:i:s', time());
		$temp['guard_id'] = Auth::user()->id;
		parent::__construct($temp);
		parent::save();

		$res = Resident::where('block_number', $temp['block_number'])->where('door_number', $temp['door_number'])->where('name', $temp['person_to_visit'])->first();
		
		if($res->gcm_token != null)
		{
			$data['notification'] = ['registration_ids' => $res->gcm_token, 'message' => [
			'type' 			=> 'Personal',
			'title' 		=> 'You have a visitor.',
			'description' 	=> 'Name : '.$temp['visitor_name'].'. Purpose of Visit : '.$temp['purpose']
			]];
            Queue::push('Common', $data);
			return true;
		}
		$data['sms'] = [
		'phone' 	=> $res->mobile,
		'message' 	=> 'You have a visitor. '.PHP_EOL.'Name : '.$temp['visitor_name'].PHP_EOL.' Purpose of Visit : '.$temp['purpose']
		];
		Queue::push('Common', $data);
		return true;
	}

	public function getReportData($key)
	{
		switch($key){
            case 'today':
                return DB::select("SELECT * FROM visitors WHERE DATE_FORMAT(date, '%Y-%m-%d') = ?", [date('Y-m-d', time())]);
                break;
            case 'guard_today' :
                return DB::select("SELECT * FROM visitors WHERE DATE_FORMAT(date, '%Y-%m-%d') = ? AND guard_id = ?", [date('Y-m-d', time()), Auth::user()->id]);
                break;
            case 'all_visitors' :
                return DB::select('SELECT * FROM visitors');
                break;
            default : return 0;
        }
	}

}