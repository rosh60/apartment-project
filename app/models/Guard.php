<?php

class Guard extends Eloquent{

	protected $fillable = ['name', 'dob', 'gender', 'laddress', 'paddress', 'mobile', 'origin', 'login', 'logout', 'position', 'photo'];
	public $timestamps = false;
	public $_input;
	public $v;

	public function __construct($data = null)
	{
		if($data != null) $this->_input = $data;
	}

	public function validateInput()
	{
		$rules = [
			'name' => 'required',
			'dob' => 'required|date',
			'gender' => 'required',
			'laddress' => 'required',
			'paddress' => 'required',
			'mobile' => 'required|numeric|max:9999999999',
			'origin' => 'required',
			'login' => 'required',
			'logout' => 'required',
			'position' => 'required',
			'photo' => 'required'
		];
		$this->v = Validator::make($this->_input, $rules);
		return $this->v->passes();
	}

	public function add()
	{
		$temp = $this->_input;
		$temp = Common::sanitize($temp);
		$temp['dob'] = date('Y-m-d', strtotime($temp['dob']));
		parent::__construct($temp);
		$flag = parent::save();
		if($flag == false) return false;
		$user = new User(['id' => $this->_uid, 'email' => $this->_input['email'], 'password' => Hash::make('test'), 'role' => '1']);
		return $user->save();
	}

}

