<?php

class Post extends eloquent
{

	protected $fillable = ['content', 'topic_id', 'uid'];

	public function topic()
	{
		return $this->belongsTo('Topic');
	}

	public function resident()
	{
		return $this->belongsTo('Resident', 'uid');
	}

}