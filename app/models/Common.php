<?php

class Common
{

	/**
	 * Sanitizes input by stripping HTML tags. Input can be an array or a string.
	 */
	public static function sanitize($value)
	{
		if(is_array($value))
		{
			foreach ($value as $key => $ele) {
				$ele = trim(strip_tags($ele));
				$value[$key] = $ele;
			}
			return $value;
		}
        $value = trim(strip_tags($value));
    	return $value;
	}

    /**
     * Fuction to process queue.
     */
    public function fire($job, $data)
    {
        if(isset($data['notification']))
        {
            $this->sendNotification($data['notification']);
        }
        if(isset($data['sms']))
        {
            $this->sendSMS($data['sms']);
        }
        if(isset($data['email']))
        {
            $data = $data['email'];
            Mail::send('emails.welcome', $data, function($message) use ($data)
            {
                $message->to($data['toId'], $data['toName'])->subject($data['subject']);
            });
        }
        $job->delete();
    }

    /**
	 * Sends notification data to GCM server.
	 */
	public static function sendNotification($data)
	{
		define("GOOGLE_API_KEY", "AIzaSyB9738EP5UhGJx99zAyYD7gydtQcMQpD-4");
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => $data['registration_ids'],
            'data' => $data['message'],
        );
        echo json_encode($data['message']);
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $str=  curl_exec($ch);
        curl_close($ch);
        return $str;
        return;
	}

	/**
	 * Sends an email using mailgun api.
	 */
	public function sendEmail($data)
	{
		$auth = base64_encode('api:key-6pzxpjwgy-md8y8he9y6fivov-4k0s88');
        $url = 'https://api.mailgun.net/v2/roshanpal.com/messages';
        $fields = array('from' =>'RoshanPal.com <contact@roshanpal.com>',
            'to' => $data['mailto'],
            'subject' => $data['subject'],
            'text' => $data['message']);
        $headers = array(
            'Authorization: key=' . 'Basic '.$auth
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_exec($ch);
        curl_close($ch);
        return;
	}

	/**
	 * Sends text messages using FullonSMS gateway.
	 * Expected input is an array containing 'phone' and 'message' fields.
	 */
	public function sendSMS($data)
	{
		$uid = '8892268175';
        $pwd = '26089';
        $curl = curl_init();
        $timeout = 30;
        $result = array();
        $headers = array();
        $phone = $data['phone'];
        $msg = $data['message'];

        $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
        $headers[] = 'Accept-Language: en-us,en;q=0.5';
        $headers[] = 'Accept-Encoding gzip,deflate';
        $headers[] = 'Keep-Alive: 300';
        $headers[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, "http://sms.fullonsms.com/login.php");
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "MobileNoLogin=".$uid."&LoginPassword=".$pwd."&x=64&y=5&red=");
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_COOKIESESSION, 1);
        curl_setopt($curl, CURLOPT_COOKIEFILE, "cookie_fullonsms");
        curl_setopt($curl, CURLOPT_COOKIEJAR,  "cookie_fullonsms");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 20);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, "iPhone 4.0");
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_REFERER, "http://sms.fullonsms.com/login.php");
        $text = curl_exec($curl);
        if (curl_errno($curl))
            return "access error : ". curl_error($curl);
        if(!stristr($text,"http://sms.fullonsms.com/landing_page.php")  && !stristr($text,"http://sms.fullonsms.com/home.php?show=contacts") &&!stristr($text, "http://sms.fullonsms.com/action_main.php") )
        {
            return "invalid login";
        }

        if (trim($msg) == "" || strlen($msg) == 0)
            return "invalid message";
        $msg = urlencode(substr($msg, 0, 160));
        $pharr = explode(",", $phone);
        $refurl = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        curl_setopt($curl, CURLOPT_REFERER, $refurl);
        curl_setopt($curl, CURLOPT_URL, "http://sms.fullonsms.com/home.php");
        $text = curl_exec($curl);

        foreach ($pharr as $p)
        {
            if (strlen($p) != 10 || !is_numeric($p) || strpos($p, ".") != false)
            {
                $result[] = array('phone' => $p, 'msg' => urldecode($msg), 'result' => "invalid number");
                continue;
            }
            $p = urlencode($p);
            curl_setopt($curl, CURLOPT_URL, 'http://sms.fullonsms.com/home.php');
            curl_setopt($curl, CURLOPT_REFERER, "http://sms.fullonsms.com/home.php?show=contacts");
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS,
                "ActionScript=%2Fhome.php&CancelScript=%2Fhome.php&HtmlTemplate=%2Fvar%2Fwww%2Fhtml%2Ffullonsms%2FStaticSpamWarning.html&MessageLength=140&MobileNos=$p&Message=$msg&Gender=0&FriendName=Your+Friend+Name&ETemplatesId=&TabValue=contacts");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $contents = curl_exec($curl);

            if(strpos($contents,"window.location.href" )  &&  strpos($contents, 'http://sms.fullonsms.com/MsgSent.php'))
            {
                curl_setopt($curl, CURLOPT_POST, 0);
                curl_setopt($curl, CURLOPT_REFERER,curl_getinfo($curl, CURLINFO_EFFECTIVE_URL));
                curl_setopt($curl, CURLOPT_URL, "http://sms.fullonsms.com/MsgSent.php");
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $contents = curl_exec($curl);
            }

            $pos = strpos($contents, 'SMS Sent successfully');
            $res = ($pos !== false) ? true : false;
            $result[] = array('phone' => $p, 'msg' => urldecode($msg), 'result' => $res);
        }
        curl_setopt($curl, CURLOPT_URL, "http://sms.fullonsms.com/logout.php?LogOut=1");
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,"1=1");
        curl_setopt($curl, CURLOPT_REFERER, "http://sms.fullonsms.com/home.php");
        curl_exec($curl);
        curl_close($curl);
        return;
	}

}